﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Hesabot.Core.Domain
{
    [TableName("Users")]
    public class User
    {
        public long Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? WebRegisterDate { get; set; }

        public string TelegramId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Ignore]
        public string Fullname
        {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

    }
}
