﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Hesabot.Core.Domain
{
    [TableName("Hashtags")]
    public class Hashtag
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public DateTime CreateDate { get; set; }

        public long? ParentId { get; set; }

        public string Description { get; set; }

        public long UserId { get; set; }
    }
}
