﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Hesabot.Core.Domain
{
    [TableName("Accounts")]
    public class Account
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public bool Active { get; set; }

        public string AccountNumber { get; set; }

        public string CardNumber { get; set; }

        public string BankName { get; set; }

        public string BranchName { get; set; }

        public string Address { get; set; }

        public long InitBalance { get; set; }

        public string Description { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime ModifyDate { get; set; }

        public long UserId { get; set; }

        public bool IsDefault { get; set; }
    }
}


