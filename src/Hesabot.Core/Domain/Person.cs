﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Hesabot.Core.Domain
{
    [TableName("Persons")]
    public class Person
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public string Email { get; set; }

        public DateTime CreateDate { get; set; }

        public string ContactNumber { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public long UserId { get; set; }
    }
}
