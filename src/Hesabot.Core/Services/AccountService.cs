﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Farasun.Library.Data;
using Hesabot.Core.Database;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Services
{
    public class AccountService: DataRepository<Account>
    {
        public AccountService()
        {
            Connection = DbFactory.Factory.GetDatabase();
        }

        #region Queries 

        private const string QueryExist = @"SELECT COUNT(Id) FROM [Accounts] WHERE [UserId] = @0 AND LOWER([Title]) = @1";

        private const string QueryAnyAccount = @"SELECT COUNT(Id) FROM [Accounts] WHERE [UserId] = @0";

        private const string QueryGetDefaultAccount = @"SELECT * FROM [Accounts] WHERE [UserId] = @0 AND [IsDefault] = 1";

        private const string QueryMyAccounts = @"SELECT * FROM [Accounts] WHERE [UserId] = @0";

        private const string QueryFindByTitle = @"SELECT * FROM [Accounts] WHERE [UserId] = @0 AND LOWER([Title]) = @1";
        #endregion


        #region Methods

        public bool AnyAccount(long userId)
        {
            return Connection.ExecuteScalar<int>(QueryAnyAccount, userId) > 0;
        }

        public void AddDefaultAccount(long userId)
        {
            var account = new Account
            {
                Active = true,
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                InitBalance = 0,
                Title = "کیف پول",
                UserId = userId
            };
            Insert(account);
        }

        public bool Exist(long userId, string title)
        {
            return Connection.ExecuteScalar<int>
                (QueryExist, userId, title.ToLower()) > 0;
        }

        public Account GetDefaultAccount(long userId)
        {
            return Connection.FirstOrDefault<Account>(QueryGetDefaultAccount, userId);
        }

        public List<Account> MyAccounts(long userId)
        {
            return Connection.Query<Account>(QueryMyAccounts, userId).ToList();
        }


        public Account FindByTitle(long userId, string title)
        {
            return Connection
                .FirstOrDefault<Account>(QueryFindByTitle, userId, title.ToLower());
        }

        #endregion
    }
}
