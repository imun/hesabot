﻿using Farasun.Library.Data;
using Hesabot.Core.Database;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Services
{
    public class PersonService: DataRepository<Person>
    {
        public PersonService()
        {
            Connection = DbFactory.Factory.GetDatabase();
        }
    }
}
