﻿using Farasun.Library.Data;
using Hesabot.Core.Database;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Services
{
    public class HashtagService: DataRepository<Hashtag>
    {
        public HashtagService()
        {
            Connection = DbFactory.Factory.GetDatabase();
        }

        #region Queries

        private const string QueryExists = @"SELECT COUNT(Id) FROM [Hashtags] WHERE LOWER(Title) = @0";

        private const string QueryGetByTitle = @"SELECT * FROM [Hashtags] WHERE LOWER(Title) = @0";

        #endregion

        public bool Exist(string hashtag)
        {
            return Connection
                       .ExecuteScalar<int>(QueryExists, hashtag.ToLower()) > 0;
        }

        public Hashtag GetByTitle(string title)
        {
            return Connection
                .FirstOrDefault<Hashtag>(QueryGetByTitle, title.ToLower());
        }
    }
}
