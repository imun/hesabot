﻿using Farasun.Library.Data;
using Hesabot.Core.Database;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Services
{
    public class UserService: DataRepository<User>
    {
        public UserService()
        {
            Connection = DbFactory.Factory.GetDatabase();
        }

        #region Queries

        private const string QueryExist = @"SELECT COUNT(Id) FROM [Users] WHERE LOWER(Username) = @0";

        private const string QueryGetByUsername = @"SELECT * FROM [Users] WHERE LOWER(Username) = @0";

        private const string QueryGetByTelegramId = @"SELECT * FROM [Users] WHERE TelegramId = @0";

        #endregion

        public bool Exist(string username)
        {
            return Connection
                .ExecuteScalar<int>(QueryExist, username.ToLower()) > 0;
        }

        public User GetByUsername(string username)
        {
            return Connection
                .FirstOrDefault<User>(QueryGetByUsername, username.ToLower());
        }


        public User GetByTelegramId(string telegramId)
        {
            return Connection
                .FirstOrDefault<User>(QueryGetByTelegramId, telegramId);
        }


    }
}
