﻿using System;
using System.Collections.Generic;
using System.Linq;
using Farasun.Library.Data;
using Hesabot.Core.Database;
using Hesabot.Core.Domain;
using Hesabot.Core.ViewModels;

namespace Hesabot.Core.Services
{
    public class TransactionService: DataRepository<Transaction>
    {

        #region Queries

        private const string QueryBetween = @"SELECT Transactions.*, Accounts.Title AS AccountTitle 
	                                            FROM Transactions INNER JOIN Accounts ON Transactions.AccountId = Accounts.Id
	                                            WHERE Transactions.UserId = @0 AND (Transactions.CreateDate >= @1 AND Transactions.CreateDate <= @2)";

        private const string QueryByMessageId = @"SELECT * FROM [Transactions] WHERE UserId = @0 AND MessageId = @1";

        private const string QueryByHashtag = @"SELECT Transactions.*, Hashtags.Id AS HashtagId, Hashtags.Title AS HashtagTitle
                                                    FROM Transactions INNER JOIN Hashtags ON Transactions.HashtagId = Hashtags.Id
                                                    WHERE Transactions.UserId = @0 AND LOWER(Hashtags.Title) = @1 AND (Transactions.CreateDate BETWEEN @2 AND @3)";

        private const string QueryAnyTransactions = @"SELECT COUNT(Id) FROM [Transactions] WHERE [UserId] = @0";

        #endregion

        public TransactionService()
        {
            Connection = DbFactory.Factory.GetDatabase();
        }


        public bool AnyTransactions(long userId)
        {
            return Connection.ExecuteScalar<int>
                (QueryAnyTransactions, userId) > 0;
        }


        public List<TransactionViewModel> ByDate(long userId, DateTime when)
        {
            var start = new DateTime(when.Year, when.Month, when.Day,0,0,0);
            var finish = new DateTime(when.Year, when.Month, when.Day, 23,59,59);
            return Between(userId, start, finish);
        }

        public List<TransactionViewModel> Between(long userId, DateTime start, DateTime finish)
        {
            start = new DateTime(start.Year, start.Month, start.Day, 0,0,0);
            finish = new DateTime(finish.Year, finish.Month, finish.Day, 23,59,59);
            return Connection
                .Query<TransactionViewModel>(QueryBetween, userId, start, finish).ToList();
        }

        public Transaction ByMessageId(long userId, long messageId)
        {
            return Connection
                .FirstOrDefault<Transaction>(QueryByMessageId, userId, messageId);
        }


        public List<TransactionViewModel> ByHashtag(long userId, string hashtag, 
            DateTime startDate, DateTime finishDate)
        {
            return Connection
                .Query<TransactionViewModel>(QueryByHashtag, userId, hashtag, startDate, finishDate)
                .ToList();
        }

    }
}
