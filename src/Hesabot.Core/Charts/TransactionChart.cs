﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Charts
{
    public class TransactionChart
    {
        public string Render(List<HashtagChartData> model)
        {
            string filename = Application.StartupPath + "\\SampleC.jpg";
            string myfile = Application.StartupPath + "\\SampleC+_.jpg";
            using (var stream = new FileStream(filename, FileMode.Create)) {
                using (var chart = new Chart()) {
                    chart.Size = new Size(600, 250);
                    chart.ChartAreas.Add(new ChartArea {
                        BackColor = Color.White
                    });
                    var s = new Series { ChartType = SeriesChartType.Bar, XValueType = ChartValueType.String };
                    foreach (var item in model) {
                        s.Points.AddXY(item.Title, item.Value);
                    }
                    chart.Series.Add(s);
                    chart.Invalidate();
                    //chart.SaveImage(stream, ChartImageFormat.Jpeg);
                    chart.SaveImage(myfile, ChartImageFormat.Jpeg);
                }
                stream.Close();
            }
            return myfile;
        }
    }
}
