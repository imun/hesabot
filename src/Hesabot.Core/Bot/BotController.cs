﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Hesabot.Core.Domain;
using Hesabot.Core.Services;
using Farasun.Library.PersianTools.PersianNumber;
using Farasun.Library.PersianTools.PersianString;
using User = Hesabot.Core.Domain.User;
using Farasun.Library.PersianTools.DateTime;
using Hesabot.Core.Charts;
using Hesabot.Core.Utils;
using Hesabot.Core.ViewModels;

namespace Hesabot.Core.Bot
{
    public class BotController
    {
        public BotController()
        {
            History = new List<BotCommand>();
        }


        #region Properties

        public List<BotCommand> History { get; set; }

        #endregion


        #region Methods

        public BotCommand Process(BotCommand command)
        {
            var result = command;
            //if (command.Username == "farzadbro") {
            //    result.OutputText = "دودو ددد آقا فرزاد";
            //    return result;
            //}

            //if (command.TelegramId == "460683906") {
            //    result.OutputText = "دودودددی آقا موصی";
            //    return result;
            //}


            if (command.Message == null)
            {
                result.OutputText = "لطفاً اطلاعات را به درستی وارد کنید!";
                return result;
            }

            if (string.IsNullOrEmpty(command.Username))
                command.Username = command.TelegramId;

            command.Message = command.Message.Trim();

            if (string.IsNullOrEmpty(command.Message))
            {
                return null;
            }

            BotCommand lastCommand = GetLastCommand(command.ChatId);

            if (command.Command.ToLower().StartsWith(BotCommandString.Start))
            {
                History.Add(command);
                result.OutputText = BotHelpString.Start;
                result.KeyboardButtons = BotMenuProvider.StartMenu(); 
                return result;
            }

            if (command.Command.StartsWith(BotCommandString.MenuFa,
                StringComparison.CurrentCultureIgnoreCase))
            {
                result.OutputText = "از منو انتخاب کنید :";
                result = CallMenu(command);
                return result;
            }

            User user = null;
            user = GetUser(command);

            command.UserId = user.Id;

            SetDefaultAccount(user.Id);

            if (command.Command == BotCommandString.CancelFa)
            {
                ClearHistory(user.Id);
                result.OutputText = "بازگشت به منوی اصلی. لطفاً انتخاب کنید : ";
                CallMenu(result);
                return result;
            }

            if (command.Command.Equals(BotMenuString.AddTransaction, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                bool anyTransactions = false;
                using (var transactionSvc = new TransactionService())
                {
                    anyTransactions = transactionSvc.AnyTransactions(user.Id);
                }
                result.OutputText = !anyTransactions 
                    ? BotHelpString.AddTransactionWithDetails 
                    : BotHelpString.AddTransaction;
                History.Add(command);
                return result;
            }
            

            if (command.Command.Trim().Equals(BotCommandString.ReportFa, StringComparison.InvariantCultureIgnoreCase) ||
                    command.Command.Trim().Equals($"/{BotCommandString.ReportFa}", StringComparison.InvariantCultureIgnoreCase))
            {
                result.KeyboardButtons = BotMenuProvider.ReportMenu();
                result.OutputText = "لطفاً بازه زمانی گزارش را انتخاب کنید : ";
                return result;
            }

            if (command.Command.StartsWith("#", StringComparison.InvariantCultureIgnoreCase))
            {
                string hashtagValue = command.Command.Substring(1, command.Command.Length - 1);
                result.OutputText = RunHashtagReport(user.Id, hashtagValue).Result;
                return result;
            }

            if (command.Command.StartsWith(BotCommandString.ReportsFa))
            {
                result.KeyboardButtons = BotMenuProvider.ReportMenu();
                result.OutputText = "انتخاب کنید :";
                return result;
            }

            if (command.Command.StartsWith(BotCommandString.ReportFa, StringComparison.InvariantCultureIgnoreCase) ||
                    command.Command.StartsWith($"/{BotCommandString.ReportFa}", StringComparison.InvariantCultureIgnoreCase))
            {

                var parameter = command.Parameters.Trim();
                if (!string.IsNullOrEmpty(parameter))
                {
                    if (parameter == BotCommandString.ReportTodayFa)
                        result.OutputText = RunTodayReport(user.Id).Result;
                    else if (parameter == BotCommandString.ReportYesterdayFa)
                        result.OutputText = RunYesterdayReport(user.Id).Result;
                    else if (parameter == BotCommandString.ReportThisWeekFa)
                        result.OutputText = RunThisWeekReport(user.Id).Result;
                    else if (parameter == BotCommandString.ReportThisMonthFa)
                        result.OutputText = RunThisMonthReport(user.Id).Result;
                    else if (parameter.StartsWith("#", StringComparison.InvariantCultureIgnoreCase))
                        result.OutputText = RunHashtagReport(user.Id, parameter.Substring(1, parameter.Length - 1)).Result;
                    else
                        result.OutputText = RunByDateReport(user.Id, parameter).Result;

                    return result;
                }
                result.OutputText = RunTodayReport(user.Id).Result;
            }


            if (command.Command.Equals(BotCommandString.ReportTodayFa,
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = RunTodayReport(user.Id).Result;
                return result;
            }

            if (command.Command.Equals(BotCommandString.ReportYesterdayFa, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = RunYesterdayReport(user.Id).Result;
                return result;
            }

            if (command.Command.Equals(BotCommandString.ReportThisWeekFa, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = RunThisWeekReport(user.Id).Result;
                return result;
            }

            if (command.Command.Equals(BotCommandString.ReportThisMonthFa, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = RunThisMonthReport(user.Id).Result;
                return result;
            }

            if (command.Command.Equals("نمودار", StringComparison.InvariantCultureIgnoreCase)) {
                var chart = new TransactionChart();
                var data = new List<HashtagChartData>
                    {
                        new HashtagChartData {Title = "مثال", Value = 1000},
                        new HashtagChartData {Title = "کوس", Value = 20000},
                        new HashtagChartData {Title = "کون", Value = 15000},
                        new HashtagChartData {Title = "عن", Value = 12000}
                    };
                //Stream ms = new FileStream(Application.StartupPath + "\\ChartFelan.jpg", FileMode.Create);
                result.ChartFilename = chart.Render(data);
                //result.ChartStream = ms;
                //ms.Close();
                return result;
            }

            //if (lastCommand != null && lastCommand.TransactionResult != null) {
            //    List<Account> myAccounts = new List<Account>();
            //    using (var accountSvc = new AccountService()) {
            //        myAccounts = accountSvc.MyAccounts(user.Id);
            //    }
            //    result.KeyboardButtons = BotMenuProvider.ChooseAccount(myAccounts);
            //    //result.Output = lastCommand.Output;
            //    return result;
            //}

            if (command.Command.StartsWith(BotCommandString.AddAccountFa, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = BotHelpString.AddAccount;
                History.Add(command);
                return result;
            }

            if(lastCommand != null && lastCommand.Command
                .StartsWith(BotCommandString.AddAccountFa, 
                StringComparison.InvariantCultureIgnoreCase))
            {
                result.OutputText = AddAccount(command).Result;
                ClearHistory(command.UserId);
                return result;
            }


            if (lastCommand != null && lastCommand.TransactionResult != null)
            {
                command.TransactionResult = lastCommand.TransactionResult;
            }


            result = AddTransaction(user.Id, command);
            
            return result;
        }

        private static User GetUser(BotCommand command)
        {
            User user = null;
            using (var userSvc = new UserService())
            {
                user = userSvc.GetByTelegramId(command.TelegramId);
                if (user == null) {
                    if (string.IsNullOrEmpty(command.Username))
                        command.Username = command.TelegramId;
                    user = new User
                    {
                        Username = command.Username,
                        CreateDate = DateTime.Now,
                        FirstName = command.FirstName,
                        LastName = command.LastName,
                        TelegramId = command.TelegramId
                    };
                    userSvc.Insert(user);
                }
            }
            return user;
        }

        private static void SetDefaultAccount(long userId)
        {
            using (var accountSvc = new AccountService())
            {
                if (!accountSvc.AnyAccount(userId))
                {
                    accountSvc.AddDefaultAccount(userId);
                }
            }
        }

        public BotCommand Edit(BotCommand command)
        {
            var result = command;

            if (command.Message == null)
            {
                result.OutputText = "لطفاً اطلاعات را به درستی وارد کنید!";
                return result;
            }

            command.Message = command.Message.Trim();

            if (string.IsNullOrEmpty(command.Message))
            {
                return null;
            }

            User user = null;
            using (var userSvc = new UserService())
            {
                //if (string.IsNullOrEmpty(command.Username))
                //{
                //    command.Username = command.TelegramId;
                //}
                user = userSvc.GetByTelegramId(command.TelegramId);
                if (user == null)
                {
                    user = new User
                    {
                        Username = command.Username,
                        CreateDate = DateTime.Now,
                        FirstName = command.FirstName,
                        LastName = command.LastName,
                        TelegramId = command.TelegramId
                    };
                    userSvc.Insert(user);
                }
            }

            try
            {
                Transaction txn;

                using (var txnSvc = new TransactionService())
                {
                    txn = txnSvc.ByMessageId(user.Id, command.MessageId);
                }

                if (txn == null)
                {
                    result.OutputText = "خطا : ویرایش تراکنش قابل انجام نیست!";
                    return result;
                }

                TransactionType txnType = TransactionType.Expense;
                var items = command.Message.Split(' ');
                if (!items.Any())
                {
                    result.OutputText = "فرمت ورودی شما اشتباه است";
                    return result;
                }
                string amountStr = items[0].ToEnglishNumber();
                long amount = 0;

                if (amountStr.StartsWith("+", StringComparison.InvariantCultureIgnoreCase))
                {
                    txnType = TransactionType.Income;
                    amountStr = amountStr.Substring(1, amountStr.Length - 1);
                }

                if (!long.TryParse(amountStr, out amount))
                {
                    result.OutputText = "عدد وارد شده اشتباه است.";
                    return result;
                }

                Hashtag hashtag = null;
                int hashtagStart = 0;
                int hashtagEnd = 0;

                if (command.Message.Contains('#'))
                {
                    var msgSize = command.Message.Length;
                    var htagPos = hashtagStart = command.Message.IndexOf('#') + 1;
                    var htagEnd = hashtagEnd = command.Message.IndexOf(' ', htagPos);
                    string htagStr = string.Empty;
                    if (htagEnd > 0)
                        htagStr = command.Message.Substring(htagPos, htagEnd - htagPos);
                    else
                        htagStr = command.Message.Substring(htagPos, msgSize - htagPos);
                    htagStr = htagStr.Trim().FixYeKe();
                    using (var hashtagSvc = new HashtagService())
                    {
                        hashtag = hashtagSvc.GetByTitle(htagStr);
                        if (hashtag == null)
                        {
                            hashtag = new Hashtag
                            {
                                CreateDate = DateTime.Now,
                                Title = htagStr,
                                UserId = user.Id
                            };
                            hashtagSvc.Insert(hashtag);
                        }
                    }
                }

                if (hashtag == null)
                {
                    using (var hashtagSvc = new HashtagService())
                    {
                        const string untagged = "UnTagged";
                        hashtag = hashtagSvc.GetByTitle(untagged);
                        if (hashtag == null)
                        {
                            hashtag = new Hashtag
                            {
                                CreateDate = DateTime.Now,
                                Title = untagged,
                                UserId = user.Id
                            };
                            hashtagSvc.Insert(hashtag);
                        }
                    }
                }

                string title = string.Empty;
                int amountSize = amountStr.Length;
                foreach (var str in items)
                {
                    if (amountStr == str ||
                        $"+{amountStr}" == str ||
                        str.Contains('#'))
                        continue;
                    title += ' ' + str;
                }

                txn = new Transaction
                {
                    Id = txn.Id,
                    Amount = amount,
                    Hashtag = hashtag.Title,
                    CreateDate = DateTime.Now,
                    Title = title.FixYeKe().Trim(),
                    HashtagId = hashtag.Id,
                    UserId = user.Id,
                    TransactionType = txnType,
                    MessageId = command.MessageId
                };

                using (var txnSvc = new TransactionService())
                {
                    txnSvc.Update(txn);
                }

                result.OutputText = $"تراکنش شماره {txn.Id} با موفقیت به روز رسانی شد.";
            }
            catch (Exception e)
            {
                result.OutputText = "مشکلی در سیستم بوجود آمده است که به زودی برطرف خواهد شد!";
                return result;
            }
            return result;

        }

        public List<BotCommand> GetCommands(long chatId)
        {
            return History
                .Where(x => x.ChatId == chatId)
                .OrderByDescending(x => x.SendTime)
                .ToList();
        }

        public BotCommand GetLastCommand(long chatId)
        {
            return History
                .FindLast(x => x.ChatId == chatId);
        }

        private string TransactionsToString(List<TransactionViewModel> model, string title = "")
        {
            StringBuilder sb = new StringBuilder();
            if (!string.IsNullOrEmpty(title))
                sb.AppendLine(title);
            long totalAmount = 0;
            foreach (var item in model)
            {
                char txnTypeChar = '-';
                if (item.TransactionType == TransactionType.Income)
                {
                    txnTypeChar = '+';
                    totalAmount += item.Amount;
                }
                else if (item.TransactionType == TransactionType.Expense)
                {
                    totalAmount -= item.Amount;
                }
                sb.AppendLine($"{txnTypeChar}{item.Amount:N0}   #{item.Hashtag}    ${item.AccountTitle}");
            }
            var sum = model.Sum(x => x.Amount);
            sb.AppendLine($"جمع مبلغ تراکنش ها : {sum:N0}");
            sb.AppendLine(Environment.NewLine);

            var incomeSum = model.Where(x => x.TransactionType == TransactionType.Income).Sum(x => x.Amount);
            sb.AppendLine($"جمع درآمد : {incomeSum:N0}+");

            var expenseSum = model.Where(x => x.TransactionType == TransactionType.Expense).Sum(x => x.Amount);
            sb.AppendLine($"جمع هزینه : {expenseSum:N0}-");

            var diffSum = incomeSum - expenseSum;
            sb.AppendLine($"تفاضل : {diffSum:N0}");

            return sb.ToString();
        }

        private BotCommonResult RunTodayReport(long userId)
        {
            BotCommonResult result = new BotCommonResult();
            using (var txnSvc = new TransactionService())
            {
                var items = txnSvc.ByDate(userId, DateTime.Now);
                string title = $"گزارش تراکنش های امروز ({PersianDateHelper.Convert(DateTime.Now, PersianDateFormat.Normal)}) :";
                result.Result = items.Any()
                    ? TransactionsToString(items, title)
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }

        private BotCommonResult RunYesterdayReport(long userId)
        {
            BotCommonResult result = new BotCommonResult();
            using (var txnSvc = new TransactionService())
            {
                var yesterday = DateTime.Now.AddDays(-1);
                var items = txnSvc.ByDate(userId, yesterday);
                string title = $"گزارش تراکنش های دیروز ({PersianDateHelper.Convert(yesterday, PersianDateFormat.Normal)}) :";
                result.Result = items.Any()
                    ? TransactionsToString(items, title)
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }

        private BotCommonResult RunThisWeekReport(long userId)
        {
            BotCommonResult result = new BotCommonResult();
            using (var txnSvc = new TransactionService())
            {
                var filterDate = PersianDateHelper
                    .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentWeek);
                var items = txnSvc.Between(userId, filterDate.Start, filterDate.Finish);
                result.Result = items.Any()
                    ? TransactionsToString(items, "گزارش تراکنش های این هفته :")
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }

        private BotCommonResult RunThisMonthReport(long userId)
        {
            BotCommonResult result = new BotCommonResult();
            using (var txnSvc = new TransactionService())
            {
                var filterDate = PersianDateHelper
                    .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentMonth);
                var items = txnSvc.Between(userId, filterDate.Start, filterDate.Finish);
                result.Result = items.Any()
                    ? TransactionsToString(items, $"گزارش تراکنش های این ماه ({filterDate.PersianMonthName})")
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }

        private BotCommonResult RunByDateReport(long userId, string value)
        {
            BotCommonResult result = new BotCommonResult();
            var myResult = DateTimeParamHelper.FindValue(value);
            if (!myResult.Success)
            {
                return myResult;
            }
            using (var txnSvc = new TransactionService())
            {
                var items = txnSvc.ByDate(userId, DateTime.Now);
                result.Result = items.Any()
                    ? TransactionsToString(items, "گزارش تراکنش های امروز :")
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }

        private BotCommonResult RunHashtagReport(long userId, string value)
        {
            BotCommonResult result = new BotCommonResult();
            var filterDate = PersianDateHelper
                    .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentMonth);
            using (var txnSvc = new TransactionService())
            {
                var items = txnSvc.ByHashtag(userId, value, 
                    filterDate.Start, filterDate.Finish);
                result.Result = items.Any()
                    ? TransactionsToString(items, $"گزارش تراکنش های هشتگ '{value}'")
                    : "نتیجه ای یافت نشد!";
            }
            return result;
        }


        private BotCommand AddTransaction(long userId, BotCommand command)
        {
            BotCommand result = command;

            try
            {
                //using (var connection = DbFactory.Factory.GetDatabase())
                //{
                    
                //}

                if (command.Command.ToLower().Trim() == BotCommandString.CancelFa)
                {
                    History.RemoveAll(x => x.UserId == command.UserId);
                    result.OutputText = "شما از ثبت تراکنش انصراف دادید.";
                    CallMenu(command);
                    return result;
                }

                 long accountId = 0;
                List<Account> myAccounts;
                using (var accountSvc = new AccountService())
                {
                    if (!accountSvc.AnyAccount(userId))
                    {
                        accountSvc.AddDefaultAccount(userId);
                    }
                    Account defaultAccount = accountSvc.GetDefaultAccount(userId);
                    if (defaultAccount != null)
                        accountId = defaultAccount.Id;

                    myAccounts = accountSvc.MyAccounts(userId);
                }

                if (command.TransactionResult != null)
                {
                    Account account;
                    using (var accountSvc = new AccountService())
                    {
                        account = accountSvc.FindByTitle(userId, result.Command) ??
                                  accountSvc.GetDefaultAccount(userId);
                    }
                    using (var hashtagSvc = new HashtagService())
                    {
                        hashtagSvc.Insert(result.TransactionResult.Hashtag);
                    }
                    using (var txnSvc = new TransactionService())
                    {
                        result.TransactionResult.Transaction.AccountId = account.Id;
                        txnSvc.Insert(result.TransactionResult.Transaction);
                    }
                    result.OutputText = "تراکنش با موفقیت ثبت شد.";
                    ClearHistory(command.UserId);
                    return result;
                }


                TransactionType txnType = TransactionType.Expense;
                var items = command.Message.Split(' ');
                if (!items.Any())
                {
                    result.OutputText = "فرمت ورودی شما اشتباه است";
                    return result;
                }
                string amountStr = items[0].ToEnglishNumber();
                long amount = 0;

                if (amountStr.StartsWith("+", StringComparison.InvariantCultureIgnoreCase))
                {
                    txnType = TransactionType.Income;
                    amountStr = amountStr.Substring(1, amountStr.Length - 1);
                }

                if (!long.TryParse(amountStr, out amount))
                {
                    result.OutputText = "عدد وارد شده اشتباه است.";
                    return result;
                }

                Hashtag hashtag = null;
                int hashtagStart = 0;
                int hashtagEnd = 0;

                if (command.Message.Contains('#'))
                {
                    var msgSize = command.Message.Length;
                    var htagPos = hashtagStart = command.Message.IndexOf('#') + 1;
                    var htagEnd = hashtagEnd = command.Message.IndexOf(' ', htagPos);
                    string htagStr = string.Empty;
                    if (htagEnd > 0)
                        htagStr = command.Message.Substring(htagPos, htagEnd - htagPos);
                    else
                        htagStr = command.Message.Substring(htagPos, msgSize - htagPos);
                    htagStr = htagStr.Trim().FixYeKe();
                    using (var hashtagSvc = new HashtagService())
                    {
                        hashtag = hashtagSvc.GetByTitle(htagStr);
                        if (hashtag == null)
                        {
                            hashtag = new Hashtag
                            {
                                CreateDate = DateTime.Now,
                                Title = htagStr,
                                UserId = userId
                            };
                            
                        }
                    }
                }

                if (hashtag == null)
                {
                    using (var hashtagSvc = new HashtagService())
                    {
                        const string untagged = "UnTagged";
                        hashtag = hashtagSvc.GetByTitle(untagged);
                        if (hashtag == null)
                        {
                            hashtag = new Hashtag
                            {
                                CreateDate = DateTime.Now,
                                Title = untagged,
                                UserId = userId
                            };
                            //hashtagSvc.Insert(hashtag);
                            result.TransactionResult.Hashtag = hashtag;
                        }
                    }
                }

                string title = string.Empty;
                int amountSize = amountStr.Length;
                foreach (var str in items)
                {
                    if (amountStr == str ||
                        $"+{amountStr}" == str ||
                        str.Contains('#'))
                        continue;
                    title += ' ' + str;
                }


                var txn = new Transaction
                {
                    Amount = amount,
                    Hashtag = hashtag.Title,
                    CreateDate = DateTime.Now,
                    Title = title.FixYeKe().Trim(),
                    HashtagId = hashtag.Id,
                    UserId = userId,
                    TransactionType = txnType,
                    MessageId = command.MessageId
                };

                result.TransactionResult = new BotTransactionResult
                {
                    Transaction = txn,
                    Hashtag = hashtag
                };


                if (accountId == 0)
                {
                    History.Add(command);
                    result.OutputText = "لطفاً حساب تراکنش را از لیست انتخاب کنید :";
                    result.KeyboardButtons = BotMenuProvider.ChooseAccount(myAccounts);
                    return result;
                }

                //History.Clear();
                //result.Output = $"تراکنش با موفقیت ثبت شد.";
                //result.Success = true;
            }
            catch (Exception e)
            {
                result.OutputText = "مشکلی در سیستم بوجود آمده است که به زودی برطرف خواهد شد!";
                result.Success = false;
                return result;
            }

            return result;
        }

        private BotTransactionResult AddTransaction(long userId, BotTransactionResult model)
        {
            return new BotTransactionResult();
        }

        private BotCommand CallMenu(BotCommand input)
        {
            BotCommand result = input;
            result.KeyboardButtons = BotMenuProvider.StartMenu();

            return result;
        }

        private BotCommonResult AddAccount(BotCommand input)
        {
            BotCommonResult result = new BotCommonResult();

            string[] array = input.Command.Split(' ');

            if (array.Length == 1)
            {
                if (array[0].IsNumber())
                {
                    result.Result = "لطفاً در ابتدا عنوان حساب و سپس مبلغ اولیه حساب را وارد نمایید.";
                    result.Success = false;
                    return result;
                }
            }

            Account account = new Account();

            foreach (var word in array)
            {
                if (word.IsNumber())
                {
                    account.InitBalance = long.Parse(word.ToEnglishNumber());
                }
                else
                {
                    account.Title += word + " ";
                }
            }
            account.Title = account.Title.FixYeKe().Trim();
            account.UserId = input.UserId;
            account.Active = true;
            account.CreateDate = account.ModifyDate = DateTime.Now;
           
            using (var accountSvc = new AccountService())
            {
                if (accountSvc.Exist(input.UserId, account.Title))
                {
                    result.Result = $"حساب با عنوان '{account.Title}' از قبل ثبت شده است. لطفاً نام حساب را تغییر داده و دوباره سعی کنید.";
                    result.Success = false;
                }
                else
                {
                    accountSvc.Insert(account);
                    result.Result = $"حساب '{account.Title}' با موجودی اولیه {account.InitBalance} برای شما ثبت شد.";
                    result.Success = true;
                }
            }

            return result;
        }


        private void ClearHistory(long userId)
        {
            History.RemoveAll(x => x.UserId == userId);
        }

        #endregion

    }
}
