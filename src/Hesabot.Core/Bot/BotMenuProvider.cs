﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.Domain;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Hesabot.Core.Bot
{
    public static class BotMenuProvider
    {
        public static ReplyKeyboardMarkup StartMenu()
        {
            return new ReplyKeyboardMarkup(new[] {
                    new[] // first row
                    {
                        new KeyboardButton(BotMenuString.AddTransaction),
                        new KeyboardButton(BotMenuString.AddAccount),
                    },
                    new[] // second row
                    {
                        new KeyboardButton(BotMenuString.Reports),
                    },
                    new[] // third row
                    {
                        new KeyboardButton(BotMenuString.GetHelp),
                        new KeyboardButton(BotMenuString.ContactUs),
                    }
                });
        }

        public static ReplyKeyboardMarkup ReportMenu()
        {
            return new ReplyKeyboardMarkup(new[]
                {
                    new[] // first row
                    {
                        new KeyboardButton(BotCommandString.ReportTodayFa),
                        new KeyboardButton(BotCommandString.ReportYesterdayFa),
                    },
                    new[] // second row
                    {
                        new KeyboardButton(BotCommandString.ReportThisWeekFa),
                        new KeyboardButton(BotCommandString.ReportThisMonthFa),
                    },

                });
        }

        public static ReplyKeyboardMarkup ChooseAccount(List<Account> model)
        {

            //var keyboard = new KeyboardButton[1][];
            var buttons = new KeyboardButton[model.Count+1];
            int i = 0;
            foreach (var account in model)
            {
                buttons[i] = (new KeyboardButton { Text = account.Title});
                i++;
            }

            buttons[model.Count] = new KeyboardButton {Text = BotCommandString.CancelFa};

            //keyboard[0] = buttons;
            return new ReplyKeyboardMarkup(buttons);
        }

    }
}
