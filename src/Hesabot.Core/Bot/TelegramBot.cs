﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Hesabot.Core.Bot
{
    public class TelegramBot
    {
        private const string _apiKey = @"337733477:AAHtz4yVf10kCDGvEdW25qQXoIvDuZaWOuI";

        private readonly TelegramBotClient Bot;
        private string ContentPath { get; set; }
        private User Me { get; set; }

        private BotController controller = new BotController();

        public TelegramBot()
        {
            //ContentPath = Application.StartupPath + "\\Content\\";
            Bot = new TelegramBotClient(_apiKey);
            Bot.OnMessage += BotOnOnMessage;
            Bot.OnMessageEdited += BotOnOnMessageEdited;
            Bot.OnInlineQuery += BotOnOnInlineQuery;
            Bot.OnCallbackQuery += BotOnOnCallbackQuery;
            Bot.OnUpdate += BotOnOnUpdate;
        }

        public async void Start()
        {
            Me = await Bot.GetMeAsync();
            Bot.StartReceiving();
        }

        private void BotOnOnUpdate(object sender, UpdateEventArgs updateEventArgs)
        {
            //TODO : do something
        }

        private void BotOnOnCallbackQuery(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            //TODO : do something
        }

        private void BotOnOnInlineQuery(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            //TODO : do something
        }

        private async void BotOnOnMessageEdited(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;
            var command = new BotCommand(message);

            var result = controller.Edit(command);

            if (result == null) return;

            await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
               replyMarkup: new ReplyKeyboardRemove());
        }

        private async void BotOnOnMessage(object sender, MessageEventArgs messageEventArgs)
        {
            //TODO : do something

            var message = messageEventArgs.Message;
            var command = new BotCommand(message);
            
            var result = controller.Process(command);

            if (result == null) return;

            if (result.InlineButtons != null)
            {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.InlineButtons);
                return;
            }

            if (result.KeyboardButtons != null)
            {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.KeyboardButtons);
                return;
            }

            //if (result.ChartStream != null)
            if(!string.IsNullOrEmpty(result.ChartFilename))
            {
                var fileName = "chart.jpg";
                using(var stream = new FileStream(result.ChartFilename, FileMode.Open)) {
                    var fts = new InputOnlineFile(stream) {
                        FileName = fileName
                    };
                    var x = await Bot.SendPhotoAsync(message.Chat.Id, fts, "نمودار تراکنش ها");
                }
                var y = await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                        replyMarkup: new ReplyKeyboardRemove());
            }
            
            await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
               replyMarkup: new ReplyKeyboardRemove());
            //await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
        }
    }
}
