﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hesabot.Core.Bot
{

    public class BotCommandString
    {
        public const string Start = @"start";
        public const string StartFa = @"شروع";
        public const string Report = @"report";
        public const string ReportFa = @"گزارش";
        public const string Reports = @"reports";
        public const string ReportsFa = @"گزارشات";
        public const string ReportToday = @"today";
        public const string ReportTodayFa = @"امروز";
        public const string ReportYesterday = @"yesterday";
        public const string ReportYesterdayFa = @"دیروز";
        public const string ReportThisWeek = @"this week";
        public const string ReportThisWeekFa = @"این هفته";
        public const string ReportThisMonth = @"this month";
        public const string ReportThisMonthFa = @"این ماه";
        public const string AddAccount = @"add account";
        public const string AddAccountFa = @"تعریف حساب";
        public const string Menu = @"menu";
        public const string MenuFa = @"منو";
        public const string Cancel = @"cancel";
        public const string CancelFa = @"انصراف";
    }

    public class BotMenuString
    {
        public const string AddTransaction = @"ثبت تراکنش";

        public const string AddAccount = @"تعریف حساب ها";

        public const string GetHelp = @"راهنما";

        public const string ContactUs = @"تماس با ما";

        public const string Reports = @"گزارشات";

        
    }

    public static class BotHelpString
    {
        public static string Start
        {
            get
            {
                return @"کار کردن و ثبت هزینه های روزانه با حسابات خیلی راحته!" + Environment.NewLine +
                       "کافیه مبلغ هزینه را بنویسید و یک هشتگ به عنوان نوع هزینه بزنید و بعد از اون هر توضیحی می خواهید بنویسید و برای بات بفرستید!" +
                       Environment.NewLine +
                       "مثلاً پیام تون یه چیزی مثل پیام زیر باید باشه :" + Environment.NewLine +
                       "50000 #خرید_روزانه از سوپر امیر" +
                       Environment.NewLine + "به همین سادگی!" + Environment.NewLine +
                       "بعد از ارسال به بات یک تراکنش هزینه به اسم خودتون ذخیره میشه که هر وقت بخواهید می تونید از این تراکنش های گزارش بگیرید و هزینه هاتون رو مدیریت کنید به همین سادگی!";
                ;
            }
        }

        public static string AddTransactionWithDetails
        {
            get
            {
                return @"ثبت تراکنش در حسابات خیلی آسون انجام میشه." + Environment.NewLine +
                       @"مبلغ تراکنش به علاوه یک هشتگ که نوع هزینه یا درآمد را مشخص میکنه به همراه یک توضیح دلخواه را برای بات بفرستید. یه چیزی شبیه به خط زیر :" +
                       Environment.NewLine + "10000 #تاکسی برای رفتن به مرکز شهر" + Environment.NewLine +
                       "به همین راحتی!";
            }
        }

        public static string AddTransaction
        {
            get
            {
                return @"تراکنش را این به صورت وارد کنید : " + Environment.NewLine +
                       "عدد #هشتگ توضیحات";
            }
        }

        public static string AddAccount
        {
            get
            {
                return
                    @"یک حساب میتونه یک حساب بانکی واقعی باشه یا حساب صندوق و دخل یا یک حساب مجازی باشه مثل کیف پول و موجودی نقد شما" +
                    Environment.NewLine +
                    @"برای تعریف حساب ابتدا نام حساب و با یک فاصله موجودی اولیه حساب را به عدد وارد کنید. اگر موجودی حساب را وارد نکنید موجودی اولیه صفر در نظر گرفته میشه." +
                    Environment.NewLine +
                    @"مثال : حساب بانک تجارت 100000 ";
            }
        }
    }
}
