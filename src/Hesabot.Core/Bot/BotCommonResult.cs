﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.Domain;

namespace Hesabot.Core.Bot
{
    public class BotCommonResult
    {

        public BotCommonResult()
        {
            Success = true;
        }

        public BotCommonResult(string result, bool success = true)
        {
            Result = result;
            Success = success;
        }

        public bool Success { get; set; }

        public string Result { get; set; }

    }


    public class BotDateResult : BotCommonResult
    {
        public DateTime? DateResult { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? FinishDate { get; set; }
    }

    public class BotTransactionResult : BotCommonResult
    {
        public Hashtag Hashtag { get; set; }

        public Transaction Transaction { get; set; }

    }
}
