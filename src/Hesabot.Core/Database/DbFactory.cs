﻿using Farasun.Library.Data.Server;
using NPoco;
using NPoco.FluentMappings;

namespace Hesabot.Core.Database
{
    public static class DbFactory
    {
        public static DatabaseFactory Factory { get; set; }

        public static void Setup()
        {
            var fluentConfig = FluentMappingConfiguration.Configure();
            //or individual mappings
            //var fluentConfig = FluentMappingConfiguration.Configure(new UserMapping(), ....);

            Factory = NPoco.DatabaseFactory.Config(x =>
            {
                x.UsingDatabase(() => new NPoco.Database(DbServer.Open("SqlServer")));
                x.WithFluentConfig(fluentConfig);
            });
        }
    }
}
