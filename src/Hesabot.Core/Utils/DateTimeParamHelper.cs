﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Farasun.Library.PersianTools.DateTime;
using Hesabot.Core.Bot;

namespace Hesabot.Core.Utils
{
    public static class DateTimeParamHelper
    {
        public static BotDateResult FindValue(string value)
        {
            BotDateResult result = new BotDateResult();
            const string spliter_error = @"جدا کننده اعداد در تاریخ پیدا نشد. از کاراکترهای - ، \ ، / و یا . برای جداسازی اعداد سال، ماه و روز استفاده کنید.";
            var slash_pos = value.IndexOf("\\");
            var dot_pos = value.IndexOf(".");
            var backSlash_pos = value.IndexOf("/");
            var dash_pos = value.IndexOf("-");
            char spliter;
            if (slash_pos > 0)
                spliter = '\\';
            else if (dot_pos > 0)
                spliter = '.';
            else if (backSlash_pos > 0)
                spliter = '/';
            else if (dash_pos > 01)
                spliter = '-';
            else
            {
                result.DateResult = null;
                result.Success = false;
                result.Result = spliter_error;
                return result;
            }

            var array = value.Split(spliter);

            if (array.Length == 2)
            {
                
            }

            if (array.Length < 3)
            {
                result = new BotDateResult
                {
                    Result = @"فرمت تاریخ اشتباه وارد شده. مثال درست : 1396-01-01",
                    Success = false,
                    DateResult = null
                };
                return result;
            }

            try
            {
                int day = int.Parse(array[0]);
                int month = int.Parse(array[1]);
                if (array[2].Length < 3)
                    array[2] = "13" + array[2];
                int year = int.Parse(array[2]);
                var calendar = new PersianDateTime(year, month,day);
                result.DateResult = calendar.ToDateTime();
                result.Success = true;
            }
            catch (Exception e)
            {
                result = new BotDateResult
                {
                    Result = @"فرمت تاریخ ورودی اشتباه است یا اعدادی که وارد کرده اید مشکل دارند!",
                    Success = false
                };
            }

            return result;
        }

        public static BotDateResult FindMonthDate(string[] value)
        {
            BotDateResult result = new BotDateResult();
            bool formatError = false;
            if (value.Length == 2)
            {
                var strMonth = value[0];
                var strYear = value[1];
                if (strYear.Length == 2)
                    strYear = "13" + strYear;
                int month = 0;
                int year = 0;
                if (!int.TryParse(strMonth, out month))
                {
                    result.Success = false;
                    result.Result = "فرمت ورودی اشتباه است.";
                    return result;
                }
                if (!int.TryParse(strYear, out year))
                {
                    result.Success = false;
                    result.Result = "فرمت ورودی اشتباه است.";
                    return result;
                }
                
            }

            return result;
        }

    }
}
