﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hesabot.Core.Utils
{
    public static class Extensions
    {
        public static bool IsNumber(this string s)
        {
            long n = 0;
            return long.TryParse(s, out n);
        }

    }
}
