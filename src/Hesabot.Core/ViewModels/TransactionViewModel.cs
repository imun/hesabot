﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.Domain;
using NPoco;

namespace Hesabot.Core.ViewModels
{
    public class TransactionViewModel
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public DateTime CreateDate { get; set; }

        public long Amount { get; set; }

        public string Hashtag { get; set; }

        public long HashtagId { get; set; }

        public long? PersonId { get; set; }

        public long UserId { get; set; }

        public string Description { get; set; }

        public TransactionType TransactionType { get; set; }

        public long MessageId { get; set; }

        public long? AccountId { get; set; }

        public string AccountTitle { get; set; }
    }
}
