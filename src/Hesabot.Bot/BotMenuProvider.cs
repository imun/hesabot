﻿using System.Collections.Generic;
using System.Linq;
using Hesabot.Core.Domain;
using Telegram.Bot.Types.ReplyMarkups;

namespace Hesabot.Bot {
    public static class BotMenuProvider {
        public static ReplyKeyboardMarkup StartMenu() {
            return new ReplyKeyboardMarkup(new[] {
                new[] // first row
                {
                    new KeyboardButton(BotMenuString.AddTransactionEmoji),
                    new KeyboardButton(BotMenuString.AddAccountEmoji)
                },
                new[] // second row
                {
                    new KeyboardButton(BotMenuString.ReportsEmoji)
                },
                new[] // third row
                {
                    new KeyboardButton(BotMenuString.GetHelpEmoji),
                    new KeyboardButton(BotMenuString.ContactUsEmoji)
                }
            });
        }

        public static ReplyKeyboardMarkup ReportMenu() {
            return new ReplyKeyboardMarkup(new[] {
                new[] // first row
                {
                    new KeyboardButton(BotCommandString.ReportTodayFa),
                    new KeyboardButton(BotCommandString.ReportYesterdayFa)
                },
                new[] // second row
                {
                    new KeyboardButton(BotCommandString.ReportThisWeekFa),
                    new KeyboardButton(BotCommandString.ReportThisMonthFa)
                }
            });
        }

        public static ReplyKeyboardMarkup ChooseAccount(List<Account> model) {
            //var keyboard = new KeyboardButton[1][];
            if (model.Count > 3)
                return ChooseAccountVertically(model);
            var buttons = new KeyboardButton[model.Count + 1];
            var i = 0;
            foreach (var account in model) {
                buttons[i] = new KeyboardButton {Text = account.Title};
                i++;
            }

            buttons[model.Count] = new KeyboardButton {Text = BotCommandString.CancelFa};

            //keyboard[0] = buttons;
            return new ReplyKeyboardMarkup(buttons);
        }

        public static ReplyKeyboardMarkup ChooseAccountVertically(List<Account> model) {
            int totalCount = model.Count;
            var keysInline = new KeyboardButton[totalCount][];
            var keysButtons = new KeyboardButton[totalCount];
            int i = 0;
            foreach(var item in model) {
                keysButtons[i] = new KeyboardButton(item.Title);
                i++;
            }

            int j = 1;
            foreach(var item in model) {
                keysInline[j - 1] = keysButtons.Take(1).ToArray();
                keysButtons = keysButtons.Skip(1).ToArray();
                j++;
            }
            if(totalCount > model.Count) {
                keysInline[j - 1] = keysButtons.Take(1).ToArray();
                keysButtons = keysButtons.Skip(1).ToArray();
            }
            return new ReplyKeyboardMarkup(keysInline);
        }
    }
}