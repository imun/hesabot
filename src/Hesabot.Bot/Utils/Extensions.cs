﻿using System;
using System.Linq;
using Farasun.Library.PersianTools.PersianNumber;
using Farasun.Library.PersianTools.PersianString;
using Hesabot.Core.Domain;
using Hesabot.Core.Domain.Base;

namespace Hesabot.Bot.Utils {
    public static class Extensions {
        public static bool IsNumber(this string s) {
            long n = 0;
            return long.TryParse(s, out n);
        }

        public static bool CheckCommandWith(this string command, string checkStr, string checkStr1 = "") {
            var isCommand = command.IndexOf("/", StringComparison.Ordinal) == 0;
            if (isCommand)
                return String.Equals(command.Substring(1, command.Length - 1).Trim(), 
                    checkStr.Trim(), StringComparison.CurrentCultureIgnoreCase) ||
                        string.Equals(command.Substring(1,command.Length -1).Trim(),
                            checkStr1.Trim(), StringComparison.CurrentCultureIgnoreCase);
            return string.Equals(command.Trim(), checkStr.Trim(), StringComparison.CurrentCultureIgnoreCase) ||
                    string.Equals(command.Trim(), checkStr1.Trim(), StringComparison.CurrentCultureIgnoreCase);
        }

        public static BotTransactionResult ExtractTransactionFromString(this string message) {
            var result = new BotTransactionResult();
            var txnType = TransactionType.Expense;
            var items = message.Split(' ');
            if (!items.Any()) {
                result.OutputMessage = BotMessages.NoMessage;
                return result;
            }

            var amountStr = items[0].ToEnglishNumber();
            if (!amountStr.IsNumber()) {
                result.OutputMessage = BotMessages.InvalidNumber;
                return result;
            }
            long amount = 0;
            if (amountStr.StartsWith("+")) {
                txnType = TransactionType.Income;
                amountStr = amountStr.Substring(1, amountStr.Length - 1);
            }

            if (!long.TryParse(amountStr, out amount)) {
                result.OutputMessage = BotMessages.InvalidNumber;
                return result;
            }

            var htagStr = string.Empty;
            var msgSize = message.Length;
            if (message.Contains('#')) {
                var htagPos = message.IndexOf('#') + 1;
                var htagEnd = message.IndexOf(' ', htagPos);
                htagStr = htagEnd > 0 ? message.Substring(htagPos, htagEnd - htagPos)
                    : message.Substring(htagPos, msgSize - htagPos);
                result.HashtagValue = htagStr = htagStr.Trim().FixYeKe();
                
            }

            var psnStr = string.Empty;
            if (message.Contains('@')) {
                var psnPos = message.IndexOf('@') + 1;
                var psnEnd = message.IndexOf(' ', psnPos);
                psnStr = psnEnd > 0 ? message.Substring(psnPos, psnEnd - psnPos)
                    : message.Substring(psnPos, msgSize - psnPos);
                result.PersonTitle = psnStr = psnStr.Trim().FixYeKe();
            }

            var dueDate = DateTime.Now;
            if(message.Contains('!')) {
                var ddtPos = message.IndexOf('!') + 1;
                var ddtEnd = message.IndexOf(' ', ddtPos);
                
            }

            var title = string.Empty;
            var amountSize = amountStr.Length;
            foreach (var str in items) {
                if (amountStr == str ||
                    $"+{amountStr}" == str ||
                                str.Contains('#') ||
                                str.Contains('@'))
                    continue;
                title += ' ' + str;
            }

            if (string.IsNullOrWhiteSpace(title))
                title = string.Empty;

            result.Transaction = new Transaction {
                Amount = amount,
                CreateDate = DateTime.Now,
                DueDate = DateTime.Now,
                HashtagValue = result.HashtagValue,
                ModifyDate = DateTime.Now,
                Status = EntityStatus.Enable,
                TransactionType = txnType,
                Title = title,
            };

            return result;
        }
    }
}