﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Farasun.Library.PersianTools.DateTime;
using Farasun.Library.PersianTools.PersianNumber;
using Farasun.Library.PersianTools.PersianString;
using Hesabot.Bot.Charts;
using Hesabot.Bot.Utils;
using Hesabot.Core.Domain;
using Hesabot.Core.Domain.Base;
using Hesabot.Core.Services;
using Hesabot.Core.ViewModels;

namespace Hesabot.Bot {
    public class BotController {
        public BotController() {
            History = new List<BotCommand>();
        }

        #region Services

        public IAccountService AccountSvc { get; set; }
        public IPersonService PersonSvc { get; set; }
        public IUserService UserSvc { get; set; }
        public ITransactionService TransactionSvc { get; set; }
        public IHashtagService HashtagSvc { get; set; }

        #endregion

        #region Properties

        public List<BotCommand> History { get; set; }

        #endregion

        #region Methods

        public BotCommand Process(BotCommand command) {
            var result = command;
            //if (command.Username == "farzadbro") {
            //    result.OutputText = "دودو ددد آقا فرزاد";
            //    return result;
            //}

            //if (command.TelegramId == "460683906") {
            //    result.OutputText = "دودودددی آقا موصی";
            //    return result;
            //}

            if (command.Message == null) {
                result.OutputText = BotMessages.NoMessage;
                return result;
            }

            if (string.IsNullOrEmpty(command.Username))
                command.Username = command.TelegramId;

            if (string.IsNullOrEmpty(command.Message)) return null;

            var lastCommand = GetLastCommand(command.ChatId);

            if (command.Command.CheckCommandWith(BotCommandString.Start, BotCommandString.StartFa)) {
                History.Add(command);
                result.OutputText = BotHelpString.Start;
                result.KeyboardButtons = BotMenuProvider.StartMenu();
                result.Mode = BotCommandMode.None;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.MenuFa, BotCommandString.Menu)) {
                result.OutputText = BotMessages.SelectFromMenu;
                result.Mode = BotCommandMode.None;
                result = CallMenu(command);
                return result;
            }

            User user = null;
            user = GetUser(command);

            command.UserId = user.Id;

            SetDefaultAccount(user.Id);

            if (command.Command.CheckCommandWith(BotCommandString.CancelFa, BotCommandString.Cancel)) {
                ClearHistory(user.Id);
                result.OutputText = BotMessages.ReturnToMenu;
                result.Mode = BotCommandMode.None;
                CallMenu(result);
                return result;
            }

            #region Edit TXN 

            if(command.Command.StartsWith("/txn_") || command.EditMode
               || (lastCommand != null && (lastCommand.EditMode 
                                           || lastCommand.Command.StartsWith("/txn_")))) {
                if (lastCommand?.TransactionResult != null)
                    command.TransactionResult = lastCommand.TransactionResult;
                result = Edit(user.Id, command);
                result.Mode = BotCommandMode.EditTransaction;
                return result;
            }

            #endregion

            if (command.Command.CheckCommandWith(BotMenuString.AddTransactionEmoji,
                BotMenuString.AddTransaction)) {
                var anyTransactions = TransactionSvc.AnyTransactions(user.Id);
                result.OutputText = !anyTransactions
                    ? BotHelpString.AddTransactionWithDetails
                    : BotHelpString.AddTransaction;
                result.Mode = BotCommandMode.None;
                History.Add(command);
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportFa, BotCommandString.Report)) {
                result.KeyboardButtons = BotMenuProvider.ReportMenu();
                result.OutputText = BotMessages.SelectReportRange;
                result.Mode = BotCommandMode.ReportSelectRange;
                return result;
            }

            if (command.Command.StartsWith("#", StringComparison.InvariantCultureIgnoreCase)) {
                var hashtagValue = command.Command.Substring(1, command.Command.Length - 1);
                result.OutputText = RunHashtagReport(user.Id, hashtagValue).Result;
                result.Mode = BotCommandMode.None;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportsFa, BotCommandString.Reports) ||
                command.Command.CheckCommandWith(BotMenuString.Reports, BotMenuString.ReportsEmoji)) {
                result.KeyboardButtons = BotMenuProvider.ReportMenu();
                result.OutputText = BotMessages.Select;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportFa, BotCommandString.Report)) {
                var parameter = command.Parameters.Trim();
                if (!string.IsNullOrEmpty(parameter)) {
                    if (parameter.CheckCommandWith( BotCommandString.ReportTodayFa, BotCommandString.ReportToday))
                        result.OutputText = RunTodayReport(user.Id).Result;
                    else if (parameter.CheckCommandWith(BotCommandString.ReportYesterdayFa, BotCommandString.ReportYesterday))
                        result.OutputText = RunYesterdayReport(user.Id).Result;
                    else if (parameter.CheckCommandWith(BotCommandString.ReportThisWeekFa, BotCommandString.ReportThisWeek))
                        result.OutputText = RunThisWeekReport(user.Id).Result;
                    else if (parameter.CheckCommandWith(BotCommandString.ReportThisMonthFa, BotCommandString.ReportThisMonth))
                        result.OutputText = RunThisMonthReport(user.Id).Result;
                    else if (parameter.StartsWith("#", StringComparison.InvariantCultureIgnoreCase))
                        result.OutputText = RunHashtagReport(user.Id, parameter.Substring(1, parameter.Length - 1))
                            .Result;
                    else
                        result.OutputText = RunByDateReport(user.Id, parameter).Result;

                    return result;
                }

                result.OutputText = RunTodayReport(user.Id).Result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportTodayFa,
                BotCommandString.ReportToday)) {
                result.OutputText = RunTodayReport(user.Id).Result;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportYesterdayFa,
                BotCommandString.ReportYesterday)) {
                result.OutputText = RunYesterdayReport(user.Id).Result;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportThisWeekFa,
                BotCommandString.ReportThisWeek)) {
                result.OutputText = RunThisWeekReport(user.Id).Result;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.ReportThisMonthFa,
                BotCommandString.ReportThisMonth)) {
                result.OutputText = RunThisMonthReport(user.Id).Result;
                return result;
            }

            if (command.Command.CheckCommandWith(BotCommandString.Chart, BotCommandString.ChartFa)) {
                var chart = new TransactionChart();
                var data = new List<HashtagChartData> {
                    new HashtagChartData {Title = "مثال", Value = 1000},
                    new HashtagChartData {Title = "تاکسی", Value = 20000},
                    new HashtagChartData {Title = "سیگار", Value = 15000},
                    new HashtagChartData {Title = "کافه", Value = 12000},
                    new HashtagChartData {Title = "سیرابی", Value = 3000},
                    new HashtagChartData {Title = "فیلان", Value = 1200}
                };
                //Stream ms = new FileStream(Application.StartupPath + "\\ChartFelan.jpg", FileMode.Create);
                result.ChartFilename = chart.Render(data);
                //result.ChartStream = ms;
                //ms.Close();
                return result;
            }

            //if (lastCommand != null && lastCommand.TransactionResult != null) {
            //    List<Account> myAccounts = new List<Account>();
            //    using (var accountSvc = new AccountService())
            //  {
            //        myAccounts = accountSvc.MyAccounts(user.Id);
            //    }
            //    result.KeyboardButtons = BotMenuProvider.ChooseAccount(myAccounts);
            //    //result.Output = lastCommand.Output;
            //    return result;
            //}

            if (command.Command.CheckCommandWith(BotCommandString.AddAccountFa,
                BotCommandString.AddAccount) || 
                command.Command.CheckCommandWith(BotMenuString.AddAccountEmoji, BotMenuString.AddAccount)) {
                result.OutputText = BotHelpString.AddAccount;
                History.Add(command);
                return result;
            }

            if (lastCommand != null && lastCommand.Command
                    .CheckCommandWith(BotCommandString.AddAccountFa,
                        BotCommandString.AddAccount) ||
                lastCommand != null && lastCommand.Command
                    .CheckCommandWith(BotMenuString.AddAccount, BotMenuString.AddAccountEmoji)) {
                result.OutputText = AddAccount(command).Result;
                ClearHistory(command.UserId);
                return result;
            }

            if (lastCommand != null && lastCommand.TransactionResult != null)
                command.TransactionResult = lastCommand.TransactionResult;

            result = AddTransaction(user.Id, command);

            return result;
        }

        private User GetUser(BotCommand command) {
            User user = null;
            user = UserSvc.GetByTelegramId(command.TelegramId);
            if (user == null) {
                if (string.IsNullOrEmpty(command.Username))
                    command.Username = command.TelegramId;
                if(!UserSvc.Exist(command.Username)) {
                    user = new User {
                        Username = command.Username,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        FirstName = command.FirstName,
                        LastName = command.LastName,
                        TelegramId = command.TelegramId,
                        Status = EntityStatus.Enable
                    };
                    user = UserSvc.Create(user).Entity;
                }
            }
            return user;
        }

        private void SetDefaultAccount(long userId) {
            if (!AccountSvc.AnyAccount(userId)) AccountSvc.AddDefaultAccount(userId);
        }

        private BotCommand AddTransaction(long userId, BotCommand command) {
            var result = command;

            try {

                if (command.Command.CheckCommandWith( BotCommandString.CancelFa,
                    BotCommandString.Cancel)) {
                    History.RemoveAll(x => x.UserId == command.UserId);
                    result.OutputText = BotMessages.CancelTransaction;
                    CallMenu(command);
                    return result;
                }

                long accountId = 0;
                List<Account> myAccounts;
                if (!AccountSvc.AnyAccount(userId)) AccountSvc.AddDefaultAccount(userId);
                var defaultAccount = AccountSvc.GetDefaultAccount(userId);
                if (defaultAccount != null)
                    accountId = defaultAccount.Id;

                myAccounts = AccountSvc.MyAccounts(userId);
                if (command.TransactionResult != null) {
                    var account = AccountSvc.FindByTitle(userId, result.Command) ??
                                  AccountSvc.GetDefaultAccount(userId);
                    var hashtagEntity = HashtagSvc.Create(result.TransactionResult.Hashtag).Entity;
                    Person personEntity = null;
                    if (command.TransactionResult.Person != null) {
                        personEntity = PersonSvc.Create(command.TransactionResult.Person).Entity;
                        result.TransactionResult.Transaction.PersonId = personEntity.Id;
                    }
                    result.TransactionResult.Transaction.AccountId = account.Id;
                    result.TransactionResult.Transaction.HashtagId = hashtagEntity.Id;
                    TransactionSvc.Create(result.TransactionResult.Transaction);
                    result.OutputText = BotMessages.TransactionAdded;
                    ClearHistory(command.UserId);
                    return result;
                }

                var txnType = TransactionType.Expense;
                var items = command.Message.Split(' ');
                if (!items.Any()) {
                    result.OutputText = BotMessages.InvalidInput;
                    return result;
                }

                var amountStr = items[0].ToEnglishNumber();
                if(!amountStr.IsNumber()) {
                    result.OutputText = BotMessages.InvalidNumber;
                    return result;
                }
                long amount = 0;
                if (amountStr.StartsWith("+")) {
                    txnType = TransactionType.Income;
                    amountStr = amountStr.Substring(1, amountStr.Length - 1);
                }

                if (!long.TryParse(amountStr, out amount)) {
                    result.OutputText = BotMessages.InvalidNumber;
                    return result;
                }

                Hashtag hashtag = null;
                var htagStr = string.Empty;
                var msgSize = command.Message.Length;
                if (command.Message.Contains('#')) {
                    var htagPos = command.Message.IndexOf('#') + 1;
                    var htagEnd = command.Message.IndexOf(' ', htagPos);
                    htagStr = htagEnd > 0 ? command.Message.Substring(htagPos, htagEnd - htagPos)
                        : command.Message.Substring(htagPos, msgSize - htagPos);
                    htagStr = htagStr.Trim().FixYeKe();
                    hashtag = HashtagSvc.GetByTitle(userId, htagStr) ?? new Hashtag {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Title = htagStr,
                        UserId = userId
                    };
                }

                Person person = null;
                var psnStr = string.Empty;
                if (command.Message.Contains('@')) {
                    var psnPos = command.Message.IndexOf('@') + 1;
                    var psnEnd = command.Message.IndexOf(' ', psnPos);
                    psnStr = psnEnd > 0 ? command.Message.Substring(psnPos, psnEnd - psnPos)
                        : command.Message.Substring(psnPos, msgSize - psnPos);
                    psnStr = psnStr.Trim().FixYeKe();
                    person = PersonSvc.GetByTitle(userId, psnStr) ?? new Person {
                        UserId = userId,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Status = EntityStatus.Enable,
                        Title = psnStr
                    };
                }

                if (hashtag == null) {
                    const string untagged = "UnTagged";
                    hashtag = HashtagSvc.GetByTitle(userId, untagged);
                    if (hashtag == null) {
                        hashtag = new Hashtag {
                            CreateDate = DateTime.Now,
                            Title = untagged,
                            UserId = userId
                        };
                        result.TransactionResult.Hashtag = hashtag;
                    }
                }

                var title = string.Empty;
                var amountSize = amountStr.Length;
                foreach (var str in items) {
                    if (amountStr == str ||
                        $"+{amountStr}" == str ||
                                    str.Contains('#') ||
                                    str.Contains('@'))
                        continue;
                    title += ' ' + str;
                }

                if (string.IsNullOrWhiteSpace(title))
                    title = string.Empty;

                var txn = new Transaction {
                    Amount = amount,
                    HashtagValue = hashtag.Title,
                    CreateDate = DateTime.Now,
                    ModifyDate = DateTime.Now,
                    DueDate = DateTime.Now,
                    Title = title.FixYeKe().Trim(),
                    UserId = userId,
                    TransactionType = txnType,
                    MessageId = command.MessageId
                };

                result.TransactionResult = new BotTransactionResult {
                    Transaction = txn,
                    Hashtag = hashtag,
                    Person = person
                };
                if (accountId == 0) {
                    History.Add(command);
                    result.OutputText = BotMessages.PlsSelectAccount;
                    result.KeyboardButtons = BotMenuProvider.ChooseAccount(myAccounts);
                    return result;
                }

                //History.Clear();
                //result.Output = $"تراکنش با موفقیت ثبت شد.";
                //result.Success = true;
            }
            catch (Exception e) {
                result.OutputText = BotMessages.CommonError;
                result.Success = false;
                return result;
            }

            return result;
        }


        public BotCommand Edit(long userId, BotCommand command) {
            var result = command;

            if (command.Message == null) {
                result.OutputText = BotMessages.NoMessage;
                return result;
            }

            try {
                if(command.Command.StartsWith("/txn_")) {
                    var txn_id = command.Command.Substring(5, command.Command.Length - 5);
                    var txnModel = TransactionSvc.Single(long.Parse(txn_id));
                    if(txnModel == null) {
                        History.RemoveAll(x => x.UserId == userId);
                        result.OutputText = string.Format(BotMessages.TransactionNotFound, txn_id);
                        CallMenu(command);
                        return result;
                    }
                    if(txnModel.UserId != userId) {
                        History.RemoveAll(x => x.UserId == userId);
                        result.OutputText = string.Format(BotMessages.TransactionNotForYou, txnModel.Id);
                        CallMenu(command);
                        return result;
                    }
                    command.EditMode = true;
                    command.TransactionResult = new BotTransactionResult {
                        Transaction = txnModel
                    };
                    History.Add(command);
                    string txnTypeChar = "-";
                    if (txnModel.TransactionType == TransactionType.Income)
                        txnTypeChar = "+";
                    result.OutputText = $"{Emojis.Pencil} ویرایش تراکنش شماره {txnModel.Id} :" + Environment.NewLine 
                                        + $"{txnModel.Amount}{txnTypeChar} #{txnModel.HashtagValue} ";
                    if(txnModel.PersonId.HasValue) {
                        result.OutputText += $"@{txnModel.Person.Title}";
                    }

                    if (!string.IsNullOrEmpty(txnModel.Title))
                        result.OutputText += $" {txnModel.Title}";
                    return result;
                }

                var lastCommand = GetLastCommand(command.ChatId);
                Transaction txn = null;
                txn = lastCommand?.TransactionResult != null 
                    ? lastCommand.TransactionResult.Transaction 
                    : TransactionSvc.ByMessageId(userId, command.MessageId);
                
                if (command.Command.ToLower().Trim() == BotCommandString.CancelFa ||
                    command.Command.ToLower().Trim() == BotCommandString.Cancel) {
                    History.RemoveAll(x => x.UserId == command.UserId);
                    result.OutputText = BotMessages.YouCanceledTxn;
                    CallMenu(command);
                    return result;
                }
                long accountId = 0;
                List<Account> myAccounts;
                if (!AccountSvc.AnyAccount(userId)) AccountSvc.AddDefaultAccount(userId);
                var defaultAccount = AccountSvc.GetDefaultAccount(userId);
                if (defaultAccount != null)
                    accountId = defaultAccount.Id;

                myAccounts = AccountSvc.MyAccounts(userId);
                if (command.TransactionResult != null && command.TransactionResult.Completed) {
                    var account = AccountSvc.FindByTitle(userId, result.Command) ??
                                  AccountSvc.GetDefaultAccount(userId);
                    Hashtag hashtagEntity = result.TransactionResult.Hashtag;
                    if(!HashtagSvc.Exist(userId, result.TransactionResult.Hashtag.Title)) {
                        hashtagEntity = HashtagSvc.Create(result.TransactionResult.Hashtag).Entity;
                    }
                    Person personEntity = null;
                    if (command.TransactionResult.Person != null) {
                        if (command.TransactionResult.Person.Id > 0)
                            result.TransactionResult.Transaction.PersonId = command.TransactionResult.Person.Id;
                        else {
                            personEntity = PersonSvc.Create(command.TransactionResult.Person).Entity;
                            result.TransactionResult.Transaction.PersonId = personEntity.Id;
                        }
                    }
                    result.TransactionResult.Transaction.AccountId = account.Id;
                    result.TransactionResult.Transaction.HashtagId = hashtagEntity.Id;
                    TransactionSvc.Update(result.TransactionResult.Transaction,
                        result.TransactionResult.Transaction.Id);
                    result.OutputText = BotMessages.TransactionUpdated;
                    ClearHistory(command.UserId);
                    return result;
                }

                var txnType = TransactionType.Expense;
                var items = command.Message.Split(' ');
                if (!items.Any()) {
                    result.OutputText = BotMessages.InvalidInput;
                    return result;
                }

                var amountStr = items[0].ToEnglishNumber();
                if (!amountStr.IsNumber()) {
                    result.OutputText = BotMessages.InvalidNumber;
                    return result;
                }
                long amount = 0;
                if (amountStr.StartsWith("+")) {
                    txnType = TransactionType.Income;
                    amountStr = amountStr.Substring(1, amountStr.Length - 1);
                }

                if (!long.TryParse(amountStr, out amount)) {
                    result.OutputText = BotMessages.InvalidNumber;
                    return result;
                }

                Hashtag hashtag = null;
                var htagStr = string.Empty;
                var msgSize = command.Message.Length;
                if (command.Message.Contains('#')) {
                    var htagPos = command.Message.IndexOf('#') + 1;
                    var htagEnd = command.Message.IndexOf(' ', htagPos);
                    htagStr = htagEnd > 0 ? command.Message.Substring(htagPos, htagEnd - htagPos)
                        : command.Message.Substring(htagPos, msgSize - htagPos);
                    htagStr = htagStr.Trim().FixYeKe();
                    hashtag = HashtagSvc.GetByTitle(userId, htagStr) ?? new Hashtag {
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Title = htagStr,
                        UserId = userId
                    };
                }

                Person person = null;
                var psnStr = string.Empty;
                if (command.Message.Contains('@')) {
                    var psnPos = command.Message.IndexOf('@') + 1;
                    var psnEnd = command.Message.IndexOf(' ', psnPos);
                    psnStr = psnEnd > 0 ? command.Message.Substring(psnPos, psnEnd - psnPos)
                        : command.Message.Substring(psnPos, msgSize - psnPos);
                    psnStr = psnStr.Trim().FixYeKe();
                    person = PersonSvc.GetByTitle(userId, psnStr) ?? new Person {
                        UserId = userId,
                        CreateDate = DateTime.Now,
                        ModifyDate = DateTime.Now,
                        Status = EntityStatus.Enable,
                        Title = psnStr
                    };
                }

                if (hashtag == null) {
                    const string untagged = "UnTagged";
                    hashtag = HashtagSvc.GetByTitle(userId, untagged);
                    if (hashtag == null) {
                        hashtag = new Hashtag {
                            CreateDate = DateTime.Now,
                            Title = untagged,
                            UserId = userId
                        };
                        result.TransactionResult.Hashtag = hashtag;
                    }
                }

                var title = string.Empty;
                var amountSize = amountStr.Length;
                foreach (var str in items) {
                    if (amountStr == str ||
                        $"+{amountStr}" == str ||
                                    str.Contains('#') ||
                                    str.Contains('@'))
                        continue;
                    title += ' ' + str;
                }

                if (string.IsNullOrWhiteSpace(title))
                    title = string.Empty;

                txn = new Transaction {
                    Id = txn.Id,
                    CreateDate = txn.CreateDate,
                    DueDate = txn.DueDate,
                    Amount = amount,
                    HashtagValue = hashtag.Title,
                    ModifyDate = DateTime.Now,
                    Title = title.FixYeKe().Trim(),
                    UserId = userId,
                    TransactionType = txnType,
                    MessageId = command.MessageId
                };

                result.TransactionResult = new BotTransactionResult {
                    Transaction = txn,
                    Hashtag = hashtag,
                    Person = person,
                    Completed = true
                };
                if (accountId == 0) {
                    command.EditMode = true;
                    History.Add(command);
                    result.OutputText = BotMessages.PlsSelectAccount;
                    result.KeyboardButtons = BotMenuProvider.ChooseAccount(myAccounts);
                    return result;
                }

            }
            catch (Exception e) {
                result.OutputText = BotMessages.CommonError;
                return result;
            }
            return result;
        }

        public List<BotCommand> GetCommands(long chatId) {
            return History
                .Where(x => x.ChatId == chatId)
                .OrderByDescending(x => x.SendTime)
                .ToList();
        }

        public BotCommand GetLastCommand(long chatId) {
            return History
                .FindLast(x => x.ChatId == chatId);
        }

        private string TransactionsToString(List<TransactionViewModel> model, string title = "") {
            var sb = new StringBuilder();
            if (!string.IsNullOrEmpty(title))
                sb.AppendLine(title);
            long totalAmount = 0;
            foreach (var item in model) {
                var txnTypeChar = '-';
                if (item.TransactionType == TransactionType.Income) {
                    txnTypeChar = '+';
                    totalAmount += item.Amount;
                }
                else if (item.TransactionType == TransactionType.Expense) {
                    totalAmount -= item.Amount;
                }

                string line = "-------------------------" + Environment.NewLine +
                     $" {item.Amount:N0}{txnTypeChar}  #{item.HashtagValue}" +  Environment.NewLine +
                     $"{Emojis.Bank} {item.AccountTitle} " + Environment.NewLine +
                              $"ویرایش : {Emojis.Pencil} /txn_{item.Id}" + Environment.NewLine;
                if (item.PersonId.HasValue)
                    line += $"  🙂{item.PersonTitle}" + Environment.NewLine;
                //line += "-------------------------" + Environment.NewLine;
                sb.AppendLine(line);
            }

            var sum = model.Sum(x => x.Amount);
            sb.AppendLine($"جمع مبلغ تراکنش ها : {sum:N0}");
            sb.AppendLine(Environment.NewLine);

            var incomeSum = model.Where(x => x.TransactionType == TransactionType.Income).Sum(x => x.Amount);
            sb.AppendLine($"جمع درآمد : {incomeSum:N0}+");

            var expenseSum = model.Where(x => x.TransactionType == TransactionType.Expense).Sum(x => x.Amount);
            sb.AppendLine($"جمع هزینه : {expenseSum:N0}-");

            var diffSum = incomeSum - expenseSum;
            sb.AppendLine($"تفاضل : {diffSum:N0}");

            return sb.ToString();
        }

        private BotCommonResult RunTodayReport(long userId) {
            var result = new BotCommonResult();
            var items = TransactionSvc.ByDate(userId, DateTime.Now);
            var title =
                $"{Emojis.Bot} گزارش تراکنش های امروز ({PersianDateHelper.Convert(DateTime.Now, PersianDateFormat.Normal)}) :";
            result.Result = items.Any()
                ? TransactionsToString(items, title)
                : BotMessages.NoResult;
            return result;
        }

        private BotCommonResult RunYesterdayReport(long userId) {
            var result = new BotCommonResult();
            var yesterday = DateTime.Now.AddDays(-1);
            var items = TransactionSvc.ByDate(userId, yesterday);
            var title = $"گزارش تراکنش های دیروز ({PersianDateHelper.Convert(yesterday, PersianDateFormat.Normal)}) :";
            result.Result = items.Any()
                ? TransactionsToString(items, title)
                : BotMessages.NoResult;
            return result;
        }

        private BotCommonResult RunThisWeekReport(long userId) {
            var result = new BotCommonResult();
            var filterDate = PersianDateHelper
                .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentWeek);
            var items = TransactionSvc.Between(userId, filterDate.Start, filterDate.Finish);
            result.Result = items.Any()
                ? TransactionsToString(items, "گزارش تراکنش های این هفته :")
                : BotMessages.NoResult;
            return result;
        }

        private BotCommonResult RunThisMonthReport(long userId) {
            var result = new BotCommonResult();
            var filterDate = PersianDateHelper
                .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentMonth);
            var items = TransactionSvc.Between(userId, filterDate.Start, filterDate.Finish);
            result.Result = items.Any()
                ? TransactionsToString(items, $"گزارش تراکنش های این ماه ({filterDate.PersianMonthName})")
                : BotMessages.NoResult;
            return result;
        }

        private BotCommonResult RunByDateReport(long userId, string value) {
            var result = new BotCommonResult();
            var myResult = DateTimeParamHelper.FindValue(value);
            if (!myResult.Success) return myResult;
            var items = TransactionSvc.ByDate(userId, DateTime.Now);
            result.Result = items.Any()
                ? TransactionsToString(items, "گزارش تراکنش های امروز :")
                : BotMessages.NoResult;
            return result;
        }

        private BotCommonResult RunHashtagReport(long userId, string value) {
            var result = new BotCommonResult();
            var filterDate = PersianDateHelper
                .GetDateFilterInfo(DateTime.Now, DateFilterType.CurrentMonth);
            var items = TransactionSvc.ByHashtag(userId, value,
                filterDate.Start, filterDate.Finish);
            result.Result = items.Any()
                ? TransactionsToString(items, $"گزارش تراکنش های هشتگ '{value}'")
                : BotMessages.NoResult;
            return result;
        }

        private BotCommand CallMenu(BotCommand input) {
            var result = input;
            result.KeyboardButtons = BotMenuProvider.StartMenu();
            return result;
        }

        private BotCommonResult AddAccount(BotCommand input) {
            var result = new BotCommonResult();

            var array = input.Command.Split(' ');

            if (array.Length == 1)
                if (array[0].IsNumber()) {
                    result.Result = BotMessages.AddAccountAmountError;
                    result.Success = false;
                    return result;
                }

            var account = new Account();
            foreach (var word in array) {
                var my_word = word.ToEnglishNumber();
                if (my_word.IsNumber())
                    account.InitBalance = long.Parse(my_word.ToEnglishNumber());
                else
                    account.Title += word + " ";
            }
                
            account.Title = account.Title.FixYeKe().Trim();
            account.UserId = input.UserId;
            account.Active = true;
            account.CreateDate = account.ModifyDate = DateTime.Now;
            if (AccountSvc.Exist(input.UserId, account.Title)) {
                result.Result = string.Format(BotMessages.AccountExist, account.Title);
                result.Success = false;
            }
            else {
                AccountSvc.Create(account);
                result.Result = string.Format(BotMessages.AddAccountSuccess, account.Title, account.InitBalance);
                result.Success = true;
            }

            return result;
        }

        private void ClearHistory(long userId) {
            History.RemoveAll(x => x.UserId == userId);
        }

        #endregion
    }
}