﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;

namespace Hesabot.Bot.Charts
{
    public class HashtagChartData
    {
        public string Title { get; set; }

        public long Value { get; set; }

    }
}
