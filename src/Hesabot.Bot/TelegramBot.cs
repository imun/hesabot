﻿using System;
using System.IO;
using Hesabot.Core.Domain;
using Hesabot.Core.Domain.Infrastructure;
using Hesabot.Core.Repository;
using Hesabot.Core.Services;
using Hesabot.Core.Services.Models;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;
using User = Telegram.Bot.Types.User;

namespace Hesabot.Bot {
    public class TelegramBot {
        private const string _apiKey = @"337733477:AAHtz4yVf10kCDGvEdW25qQXoIvDuZaWOuI";

        private readonly TelegramBotClient Bot;
        private string ContentPath { get; set; }
        private User Me { get; set; }

        private BotController controller { get; }

        public TelegramBot() {
            //ContentPath = Application.StartupPath + "\\Content\\";
            ConfigureServices();
            Bot = new TelegramBotClient(_apiKey);
            controller = new BotController {
                TransactionSvc = ServiceProvider.GetService<ITransactionService>(),
                HashtagSvc = ServiceProvider.GetService<IHashtagService>(),
                AccountSvc = ServiceProvider.GetService<IAccountService>(),
                PersonSvc = ServiceProvider.GetService<IPersonService>(),
                UserSvc = ServiceProvider.GetService<IUserService>()
            };
            Bot.OnMessage += BotOnOnMessage;
            Bot.OnMessageEdited += BotOnOnMessageEdited;
            Bot.OnInlineQuery += BotOnOnInlineQuery;
            Bot.OnCallbackQuery += BotOnOnCallbackQuery;
            Bot.OnUpdate += BotOnOnUpdate;
        }

        public async void Start() {
            Me = await Bot.GetMeAsync();
            Bot.StartReceiving();
        }

        private void BotOnOnUpdate(object sender, UpdateEventArgs updateEventArgs) {
            //TODO : do something
        }

        private void BotOnOnCallbackQuery(object sender, CallbackQueryEventArgs callbackQueryEventArgs) {
            //TODO : do something
        }

        private void BotOnOnInlineQuery(object sender, InlineQueryEventArgs inlineQueryEventArgs) {
            //TODO : do something
        }

        private async void BotOnOnMessageEdited(object sender, MessageEventArgs messageEventArgs) {
            var message = messageEventArgs.Message;
            var command = new BotCommand(message);
            
            var result = controller.Process(command);

            if (result == null) return;

            if (result.InlineButtons != null) {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.InlineButtons);
                return;
            }

            if (result.KeyboardButtons != null) {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.KeyboardButtons);
                return;
            }

            await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                replyMarkup: new ReplyKeyboardRemove());
        }

        private async void BotOnOnMessage(object sender, MessageEventArgs messageEventArgs) {
            //TODO : do something

            var message = messageEventArgs.Message;
            var command = new BotCommand(message);

            var result = controller.Process(command);

            if (result == null) return;

            if (result.InlineButtons != null) {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.InlineButtons);
                return;
            }

            if (result.KeyboardButtons != null) {
                await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: result.KeyboardButtons);
                return;
            }

            //if (result.ChartStream != null)
            if (!string.IsNullOrEmpty(result.ChartFilename)) {
                var fileName = "chart.jpg";
                using (var stream = new FileStream(result.ChartFilename, FileMode.Open)) {
                    var fts = new InputOnlineFile(stream) {
                        FileName = fileName
                    };
                    var x = await Bot.SendPhotoAsync(message.Chat.Id, fts, "نمودار تراکنش ها");
                }

                var y = await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                    replyMarkup: new ReplyKeyboardRemove());
            }

            await Bot.SendTextMessageAsync(message.Chat.Id, result.OutputText,
                replyMarkup: new ReplyKeyboardRemove());
            //await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);
        }


        private static IServiceProvider ServiceProvider { get; set; }

        private static void ConfigureServices() {
            ServiceProvider = new ServiceCollection()
                .AddSingleton<HesabotContext>()
                .AddTransient<IValidationResult, ValidationResult>()
                .AddSingleton<IHesabotRepository<Transaction>, HesabotRepository<Transaction>>()
                .AddSingleton<IHesabotRepository<Person>, HesabotRepository<Person>>()
                .AddSingleton<IHesabotRepository<Account>, HesabotRepository<Account>>()
                .AddSingleton<IHesabotRepository<Hashtag>, HesabotRepository<Hashtag>>()
                .AddSingleton<IHesabotRepository<Core.Domain.User>, HesabotRepository<Core.Domain.User>>()

                .AddSingleton<ITransactionService, TransactionService>()
                .AddSingleton<IPersonService, PersonService>()
                .AddSingleton<IAccountService, AccountService>()
                .AddSingleton<IHashtagService, HashtagService>()
                .AddSingleton<IUserService, UserService>()

                .BuildServiceProvider();
        }
    }
}