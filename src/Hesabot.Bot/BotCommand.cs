﻿using System;
using System.Collections.Generic;
using System.IO;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace Hesabot.Bot {
    public enum BotCommandMode {
        None = 0,
        AddTransaction,
        EditTransaction,
        DeleteTransaction,
        ReportSelectRange,
    }

    public class BotCommand {
        public BotCommand() {
        }

        public BotCommand(Message message) {
            Message = message.Text;
            ChatId = message.Chat.Id;
            Username = message.From.Username;
            FirstName = message.From.FirstName;
            LastName = message.From.LastName;
            TelegramId = message.From.Id.ToString();
            MessageId = message.MessageId;
            EditMode = message.EditDate.HasValue;
        }

        public long Id { get; set; }

        public DateTime SendTime { get; set; }

        public BotCommandMode Mode { get; set; }

        public bool EditMode { get; set; }

        public string Message { get; set; }

        public bool IsCommand => Message.IndexOf("/", StringComparison.Ordinal) == 0;

        public string Command {
            get {
                try {
                    var idx = Message.IndexOf("/", StringComparison.Ordinal);
                    //if (idx == 0)
                    //    return Message.Substring(1, Message.Length - 1).ToLower().Trim();
                    if (idx == -1)
                        return Message.ToLower().Trim();
                    var first_space = Message.IndexOf(" ", StringComparison.Ordinal);
                    var hasSpace = first_space > -1;
                    if (hasSpace)
                        return Message.Substring(idx, first_space + 1);
                    return Message.Substring(idx, Message.Length).ToLower().Trim();
                }
                catch {
                    return string.Empty;
                }
            }
        }

        public long ChatId { get; set; }

        public string OutputText { get; set; }

        public List<string> Params { get; set; }

        public string Parameters {
            get {
                try {
                    if (Command == string.Empty)
                        return string.Empty;

                    if (Command.StartsWith("/", StringComparison.CurrentCultureIgnoreCase)) {
                        if (Message.Trim() == Command.Trim())
                            return string.Empty;

                        return Message.Substring(Command.Length,
                            Message.Length - Command.Length).Trim();
                    }

                    var first_space = Message.IndexOf(" ");
                    return Message.Substring(first_space,
                        Message.Length - first_space).Trim();
                }
                catch {
                    return string.Empty;
                }
            }
        }

        public bool HasError { get; set; }

        public string ErrorMessage { get; set; }

        public string Username { get; set; }

        public long UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string TelegramId { get; set; }

        public long MessageId { get; set; }

        public InlineKeyboardMarkup InlineButtons { get; set; }

        public ReplyKeyboardMarkup KeyboardButtons { get; set; }

        public Stream ChartStream { get; set; }

        public string ChartFilename { get; set; }

        public BotTransactionResult TransactionResult { get; set; }

        public bool Success { get; set; }
    }
}