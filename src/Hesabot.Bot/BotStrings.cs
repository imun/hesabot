﻿using System;
using Hesabot.Bot.Utils;

namespace Hesabot.Bot {
    public class BotCommandString {
        public const string Start = @"start";
        public const string StartFa = @"شروع";
        public const string Report = @"report";
        public const string ReportFa = @"گزارش";
        public const string Reports = @"reports";
        public const string ReportsFa = @"گزارشات";
        public const string ReportToday = @"today";
        public const string ReportTodayFa = @"امروز";
        public const string ReportYesterday = @"yesterday";
        public const string ReportYesterdayFa = @"دیروز";
        public const string ReportThisWeek = @"this week";
        public const string ReportThisWeekFa = @"این هفته";
        public const string ReportThisMonth = @"this month";
        public const string ReportThisMonthFa = @"این ماه";
        public const string AddAccount = @"add account";
        public const string AddAccountFa = @"تعریف حساب";
        public const string Menu = @"menu";
        public const string MenuFa = @"منو";
        public const string Cancel = @"cancel";
        public const string CancelFa = @"انصراف";
        public const string Chart = "chart";
        public const string ChartFa = "نمودار";
    }

    public static class BotMenuString {
        public const string AddTransactionEmoji = Emojis.Heavy_Plus_Sign + @" ثبت تراکنش";
        public const string AddTransaction = "ثبت تراکنش";
        public const string AddAccountEmoji = Emojis.Bank + @" تعریف حساب";
        public const string AddAccount = "تعریف حساب";
        public const string GetHelpEmoji = Emojis.Question + @" راهنما";
        public const string GetHelp = "راهنما";
        public const string ContactUsEmoji = Emojis.Telephone + @" تماس با ما";
        public const string ContactUs = "تماس با ما";
        public const string ReportsEmoji = Emojis.Clipboard + @" گزارشات";
        public const string Reports = "گزارشات";
    }

    public static class BotMessages {
        public const string InvalidInput = "فرمت ورودی شما اشتباه است";
        public const string InvalidNumber = "عدد وارد شده اشتباه است.";
        public const string CommonError = "مشکلی در سیستم بوجود آمده است که به زودی برطرف خواهد شد!";
        public const string PlsSelectAccount = "لطفاً حساب تراکنش را از لیست انتخاب کنید :";
        public const string NoMessage = "لطفاً اطلاعات را به درستی وارد کنید!";
        public const string SelectFromMenu = "از منو انتخاب کنید :";
        public const string YouCanceledTxn = "شما از ثبت تراکنش انصراف دادید.";
        public const string TransactionAdded = "تراکنش با موفقیت ثبت شد.";
        public const string TransactionUpdated = "تراکنش با موفقیت به روزرسانی شد.";
        public const string SelectReportRange = "لطفاً بازه زمانی گزارش را انتخاب کنید یا تاریخ را به صورت رشته وارد کنید : \n مثال : 01-01-1398";
        public const string Select = "انتخاب کنید :";
        public const string ReturnToMenu = "بازگشت به منوی اصلی. لطفاً انتخاب کنید : ";
        public const string CancelTransaction = "شما از ثبت تراکنش انصراف دادید.";
        public const string NoResult = Emojis.Bot + " نتیجه ای یافت نشد!";
        public const string AddAccountAmountError = "لطفاً در ابتدا عنوان حساب و سپس مبلغ اولیه حساب را وارد نمایید.";
        public const string AccountExist = "حساب با عنوان '{0}' از قبل ثبت شده است. لطفاً نام حساب را تغییر داده و دوباره سعی کنید.";
        public const string AddAccountSuccess = "حساب '{0}' با موجودی اولیه {1} برای شما ثبت شد.";
        public const string TransactionNotFound = "تراکنش با شناسه '{0}' یافت نشد!";
        public const string TransactionNotForYou = "تراکنش با شناسه '{0}' متعلق به شما نیست که ویرایش کنید! لطفاً شیطونی نکنید 😜";
    }

    public static class BotHelpString {
        public static string Start =>
            $"{Emojis.Moneybag} {Emojis.Dollar} " + @"کار کردن و ثبت هزینه های روزانه با حسابات خیلی راحته!" +
            Environment.NewLine +
            "کافیه مبلغ هزینه را بنویسید و یک هشتگ به عنوان نوع هزینه بزنید و بعد از اون هر توضیحی می خواهید بنویسید و برای بات بفرستید!" +
            Environment.NewLine +
            "مثلاً پیام تون یه چیزی مثل پیام زیر باید باشه :" + Environment.NewLine +
            "50000 #خرید_روزانه از سوپر امیر" +
            Environment.NewLine + "به همین سادگی!" + Environment.NewLine +
            "بعد از ارسال به بات یک تراکنش هزینه به اسم خودتون ذخیره میشه که هر وقت بخواهید می تونید از این تراکنش های گزارش بگیرید و هزینه هاتون رو مدیریت کنید به همین سادگی!";

        public static string AddTransactionWithDetails =>
            $"{Emojis.Heavy_Plus_Sign} " + @"ثبت تراکنش در حسابات خیلی آسون انجام میشه." + Environment.NewLine +
            @"مبلغ تراکنش به علاوه یک هشتگ که نوع هزینه یا درآمد را مشخص میکنه به همراه یک توضیح دلخواه را برای بات بفرستید. یه چیزی شبیه به خط زیر :" +
            Environment.NewLine + "10000 #تاکسی برای رفتن به مرکز شهر" + Environment.NewLine +
            "به همین راحتی!";

        public static string AddTransaction =>
            $"{Emojis.Cyclone}" + @"تراکنش را این به صورت وارد و برای بات ارسال کنید : " + Environment.NewLine +
            "عدد #هشتگ @نام_شخص توضیحات" + Environment.NewLine +
            @"مثال : 12000 #قرض @علی قراره 2 روز دیگه برگردونه!";

        public static string AddAccount =>
            @"یک حساب میتونه یک حساب بانکی واقعی باشه یا حساب صندوق و دخل یا یک حساب مجازی باشه مثل کیف پول و موجودی نقد شما" +
            Environment.NewLine +
            @"برای تعریف حساب ابتدا نام حساب و با یک فاصله موجودی اولیه حساب را به عدد وارد کنید. اگر موجودی حساب را وارد نکنید موجودی اولیه صفر در نظر گرفته میشه." +
            Environment.NewLine +
            @"مثال : بانک تجارت 100000 ";
    }
}