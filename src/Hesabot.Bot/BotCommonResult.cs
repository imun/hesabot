﻿using System;
using Hesabot.Core.Domain;

namespace Hesabot.Bot {
    public class BotCommonResult {
        public BotCommonResult() {
            Success = true;
        }

        public BotCommonResult(string result, bool success = true) {
            Result = result;
            Success = success;
        }

        public bool Success { get; set; }

        public string Result { get; set; }
    }

    public class BotDateResult : BotCommonResult {
        public DateTime? DateResult { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? FinishDate { get; set; }
    }

    public class BotTransactionResult : BotCommonResult {
        public Hashtag Hashtag { get; set; }
        public Transaction Transaction { get; set; }
        public Person Person { get; set; }
        public bool Completed { get; set; }
        public string OutputMessage { get; set; }
        public string HashtagValue { get; set; }
        public string PersonTitle { get; set; }
    }
}