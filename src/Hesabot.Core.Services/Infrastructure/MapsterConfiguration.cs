﻿using Hesabot.Core.Domain;
using Hesabot.Core.ViewModels;
using Mapster;

namespace Hesabot.Core.Services.Infrastructure {
    public static class MapsterConfiguration {
        public static void Configure() {
            TypeAdapterConfig<Transaction, TransactionViewModel>.NewConfig()
                .Map(dest => dest.AccountTitle, src => src.AccountId.HasValue
                    ? src.Account.Title
                    : string.Empty)
                .Map(dest => dest.PersonTitle, src => src.PersonId.HasValue
                    ? src.Person.Title
                    : string.Empty);
        }
    }
}
