﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hesabot.Core.Domain;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;

namespace Hesabot.Core.Services {
    public interface IPersonService: IServiceBase<Person> {
        bool Exist(long userId, string title);
        Person GetByTitle(long userId, string title);
    }

    public class PersonService: ServiceBase<Person>, IPersonService {
        public PersonService(IHesabotRepository<Person> repository, IValidationResult validationResult) 
            : base(repository, validationResult) {
        }

        public override bool Validate(Person model, ValidationMode mode = ValidationMode.Create) {
            return true;
        }

        public bool Exist(long userId, string title) {
            return _repository
                .Query(x => x.UserId == userId && x.Title.ToLower() == title.ToLower())
                .Any();
        }

        public Person GetByTitle(long userId, string title) {
            return _repository.Select()
                .FirstOrDefault(x => x.UserId == userId && x.Title.ToLower() == title.ToLower());
        }
    }
}
