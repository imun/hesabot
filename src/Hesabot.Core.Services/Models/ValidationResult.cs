﻿using System.Collections.Generic;

namespace Hesabot.Core.Services.Models {
    public enum ValidationMode {
        Create = 0,
        Update = 1,
        Delete = 2
    }

    public interface IValidationResult {
        void AddError(string key, string errorMessage);
        bool IsValid { get; }
        string Message { get; set; }
    }

    public class ValidationResult : IValidationResult {

        private Dictionary<string, string> _errors;

        public ValidationResult() {
            _errors = new Dictionary<string, string>();
        }
        public string Message { get; set; }
        public bool IsValid { get; set; }

        public void AddError(string key, string errorMessage) {
            _errors.Add(key, errorMessage);
        }
    }
}
