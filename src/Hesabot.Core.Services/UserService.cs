﻿using System.Linq;
using Hesabot.Core.Domain;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;

namespace Hesabot.Core.Services {
    public interface IUserService: IServiceBase<User> {
        bool Exist(string username);
        User GetByUsername(string username);
        User GetByTelegramId(string telegramId);
    }
    public class UserService: ServiceBase<User>, IUserService {
        public UserService(IHesabotRepository<User> repository, IValidationResult validationResult) 
            : base(repository, validationResult) {
        }

        public override bool Validate(User model, ValidationMode mode = ValidationMode.Create) {
            return true;
        }

        public bool Exist(string username) {
            return _repository.Query(x => x.Username.ToLower() == username.ToLower()).Any();
        }

        public User GetByUsername(string username) {
            return _repository.Select()
                .SingleOrDefault(x => x.Username.ToLower() == username.ToLower());
        }

        public User GetByTelegramId(string telegramId) {
            return _repository.Select()
                .FirstOrDefault(x => x.TelegramId == telegramId);
        }
    }
}
