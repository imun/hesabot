﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hesabot.Core.Domain;
using Hesabot.Core.Domain.Base;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;

namespace Hesabot.Core.Services {
    public interface IAccountService : IServiceBase<Account> {
        bool AnyAccount(long userId);
        Account AddDefaultAccount(long userId, string title = "کیف پول");
        bool Exist(long userId, string title);
        Account GetDefaultAccount(long userId);
        List<Account> MyAccounts(long userId);
        Account FindByTitle(long userId, string title);
    }

    public class AccountService: ServiceBase<Account>, IAccountService {
        public AccountService(IHesabotRepository<Account> repository, IValidationResult validationResult) 
            : base(repository, validationResult) {
        }

        public override bool Validate(Account model, ValidationMode mode = ValidationMode.Create) {
            return true;
        }

        public bool AnyAccount(long userId) {
            return _repository
                .Query(x => x.UserId == userId).Any();
        }

        public Account AddDefaultAccount(long userId, string title = "کیف پول") {
            var account = new Account {
                Active = true,
                CreateDate = DateTime.Now,
                ModifyDate = DateTime.Now,
                InitBalance = 0,
                Title = title,
                UserId = userId,
                Status = EntityStatus.Enable
            };
            var result = Create(account);
            return result.Entity;
        }

        public bool Exist(long userId, string title) {
            return _repository
                .Query(x => x.UserId == userId && x.Title.ToLower() == title.ToLower()).Any();
        }

        public Account GetDefaultAccount(long userId) {
            return _repository
                .Query(x => x.UserId == userId && x.IsDefault)
                .SingleOrDefault();
        }

        public List<Account> MyAccounts(long userId) {
            return _repository
                .Query(x => x.UserId == userId)
                .ToList();
        }

        public Account FindByTitle(long userId, string title) {
            return _repository
                .Query(x => x.UserId == userId && x.Title.ToLower() == title.ToLower())
                .FirstOrDefault();
        }


    }
}
