﻿using System.Linq;
using Hesabot.Core.Domain;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;

namespace Hesabot.Core.Services {
    public interface IHashtagService: IServiceBase<Hashtag> {
        bool Exist(long userId, string title);
        Hashtag GetByTitle(long userId, string title);
    }

    public class HashtagService: ServiceBase<Hashtag>, IHashtagService {
        public HashtagService(IHesabotRepository<Hashtag> repository, IValidationResult validationResult) 
            : base(repository, validationResult) {
        }

        public override bool Validate(Hashtag model, ValidationMode mode = ValidationMode.Create) {
            return true;
        }

        public bool Exist(long userId, string title) {
            return _repository
                .Query(x => x.UserId == userId && x.Title.ToLower() == title.ToLower())
                .Any();
        }

        public Hashtag GetByTitle(long userId, string title) {
            return _repository.Select()
                .FirstOrDefault(x => x.UserId == userId && x.Title.ToLower() == title.ToLower());
        }

    }
}
