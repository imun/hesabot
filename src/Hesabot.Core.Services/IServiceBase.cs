﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hesabot.Core.Domain.Base;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;

namespace Hesabot.Core.Services {
    public interface IServiceBase<T> where T: BaseEntity {
        ServiceActionResult<T> Create(T model);
        ServiceActionResult<T> Update(T model, object id);
        ServiceActionResult<T> Delete(T model);
        List<T> FindAll();
        List<T> Query(Expression<Func<T, bool>> expression);
        Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression);
        T Single(object id);
        Task<T> SingleAsync(object id);
        bool Validate(T model, ValidationMode mode = ValidationMode.Create);
        IQueryable<T> Select();
    }

    public class ServiceBase<T> : IServiceBase<T> where T: BaseEntity {
        protected IValidationResult _validationResult;
        protected IHesabotRepository<T> _repository;

        public ServiceBase(IHesabotRepository<T> repository, IValidationResult validationResult) {
            _validationResult = validationResult;
            _repository = repository;
        }

        public virtual ServiceActionResult<T> Create(T model) {
            var result = new ServiceActionResult<T>();
            if (Validate(model, ValidationMode.Create)) {
                _repository.Add(model);
                result.Entity = model;
                result.Success = true;
            }
            else {
                result.Success = false;
                result.Message = _validationResult.Message;
            }

            return result;
        }

        public virtual List<T> FindAll() {
            return new List<T>(_repository.FindAll());
        }

        public virtual List<T> Query(Expression<Func<T, bool>> expression) {
            return new List<T>(_repository.Query(expression));
        }

        public virtual async Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression) {
            return await _repository.QueryAsync(expression);
        }

        public virtual T Single(object id) {
            return _repository.Single(id);
        }

        public virtual async Task<T> SingleAsync(object id) {
            return await _repository.SingleAsync(id);
        }

        public virtual ServiceActionResult<T> Delete(T model) {
            var result = new ServiceActionResult<T>();
            if (Validate(model, ValidationMode.Delete)) {
                _repository.Delete(model);
                result.Success = true;
                result.Entity = model;
            }
            else {
                result.Success = false;
                result.Message = _validationResult.Message;
            }
            return result;
        }

        public virtual ServiceActionResult<T> Update(T model, object id) {
            var result = new ServiceActionResult<T>();
            if (Validate(model, ValidationMode.Update)) {
                _repository.Update(model, id);
                result.Success = true;
                result.Entity = model;
            }
            else {
                result.Success = false;
                result.Message = _validationResult.Message;
            }
            return result;
        }

        public virtual bool Validate(T model, ValidationMode mode = ValidationMode.Create) {
            return _validationResult.IsValid;
        }

        public virtual IQueryable<T> Select() {
            return _repository.Select();
        }
    }
}
