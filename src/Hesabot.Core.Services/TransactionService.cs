﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Hesabot.Core.Domain;
using Hesabot.Core.Repository;
using Hesabot.Core.Services.Models;
using Hesabot.Core.ViewModels;
using Mapster;

namespace Hesabot.Core.Services {
    public interface ITransactionService : IServiceBase<Transaction> {
        bool AnyTransactions(long userId);
        List<TransactionViewModel> ByDate(long userId, DateTime when);
        List<TransactionViewModel> Between(long userId, DateTime start, DateTime finish);
        Transaction ByMessageId(long userId, long messageId);
        List<TransactionViewModel> ByHashtag(long userId, string hashtag,
            DateTime startDate, DateTime finishDate);
        IList<TransactionViewModel> ByHashtag(long userId, string hashtagValue);
        IList<TransactionViewModel> ByHashtag(long userId, string hashtagValue, DateTime start, DateTime? finish);
    }
    public class TransactionService: ServiceBase<Transaction>, ITransactionService {
        public TransactionService(IHesabotRepository<Transaction> repository, IValidationResult validationResult) 
            : base(repository, validationResult) {
        }

        public override bool Validate(Transaction model, ValidationMode mode = ValidationMode.Create) {
            return true;
        }

        public bool AnyTransactions(long userId) {
            return _repository.Query(x => x.UserId == userId).Any();
        }

        public List<TransactionViewModel> ByDate(long userId, DateTime when) {
            var start = new DateTime(when.Year, when.Month, when.Day, 0, 0, 0);
            var finish = new DateTime(when.Year, when.Month, when.Day, 23, 59, 59);
            return Between(userId, start, finish);
        }

        public List<TransactionViewModel> Between(long userId, DateTime start, DateTime finish) {
            return _repository
                .Query(x => x.UserId == userId && x.DueDate >= start 
                                                             && x.DueDate <= finish)
                .Select(x=> x.Adapt<TransactionViewModel>())
                .ToList();
        }

        public Transaction ByMessageId(long userId, long messageId) {
            return _repository.Select()
                .SingleOrDefault(x => x.UserId == userId && x.MessageId == messageId);
        }

        /// <summary>
        /// Fetch User's Teransaction based on Hahstag and between dates
        /// </summary>
        /// <param name="userId">Which user?</param>
        /// <param name="hashtag">Hashtag title</param>
        /// <param name="startDate">Transactions greater than startDate</param>
        /// <param name="finishDate">Transactions between startDate and finishDate</param>
        /// <returns>List of transactions</returns>
        public List<TransactionViewModel> ByHashtag(long userId, string hashtag, DateTime startDate, DateTime finishDate) {
            return _repository.Select()
                .Include(x => x.Hashtag)
                .Where(x=> x.UserId == userId)
                .Where(x=> x.DueDate >= startDate && x.DueDate <= finishDate)
                .Where(x=> x.Hashtag.Title.ToLower().Trim() == hashtag.ToLower().Trim())
                .Select(x=> x.Adapt<TransactionViewModel>())
                .ToList();
        }

        /// <summary>
        /// Return User's transactions based on Hahstag's Title
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="hashtagValue"></param>
        /// <returns></returns>
        public IList<TransactionViewModel> ByHashtag(long userId, string hashtagValue) {
            return _repository.Select()
                .Include(x => x.Hashtag)
                .Where(x=> x.UserId == userId)
                .Where(x => x.Hashtag.Title.ToLower().Trim() == hashtagValue.ToLower().Trim())
                .Select(x=> x.Adapt<TransactionViewModel>())
                .ToList();
        }

        /// <summary>
        /// List of User's transactions between start and finish date based on Hahstag title
        /// </summary>
        /// <param name="userId">Id of user that has created the transaction</param>
        /// <param name="hashtagValue">Hashtag title</param>
        /// <param name="start">When to start based on DueDate</param>
        /// <param name="finish">When to finish based on DueDate</param>
        /// <returns></returns>
        public IList<TransactionViewModel> ByHashtag(long userId, string hashtagValue, DateTime start,
            DateTime? finish) {
            var query = _repository.Select()
                .Include(x => x.Hashtag)
                .Where(x=> x.UserId == userId)
                .Where(x => x.Hashtag.Title.ToLower().Trim() == hashtagValue.ToLower().Trim())
                .Where(x => x.DueDate >= start);
            if (finish.HasValue)
                query = query.Where(x => x.DueDate <= finish);
            return _repository.Select()
                .Select(x => x.Adapt<TransactionViewModel>())
                .ToList();
        }
    }
}
