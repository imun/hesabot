﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hesabot.Core.Domain;
using Hesabot.Core.Services;
using Hesabot.Core.ViewModels;
using Hesabot.Core.ViewModels.Api;
using Microsoft.AspNetCore.Mvc;
using Mapster;

namespace Hesabot.Api.Areas.v01.Controllers {
    public class TransactionsController : Controller {
        private ITransactionService _service;

        public TransactionsController(ITransactionService service) {
            _service = service;
            
        }

        [HttpPost]
        [Route("get/{id}")]
        public async Task<ApiResultViewModel<TransactionViewModel>> Single(int id) {
            var entity = await _service.SingleAsync(id);
            var result = new ApiResultViewModel<TransactionViewModel> {
                model = entity.Adapt<TransactionViewModel>(),
                success = true,
                message = $"Transaction with ID: {id} fetched."
            };
            return result;
        }

        [HttpPost]
        [Route("Index/{userId}/{start}/{finish?}")]
        public ApiListResultViewModel<TransactionViewModel> Index(long userId, DateTime start, DateTime? finish) {
            var result = new ApiListResultViewModel<TransactionViewModel> {
                items = finish.HasValue
                    ? _service.Between(userId, start, finish.Value)
                    : _service.ByDate(userId, start),
                success = true
            };
            result.message = $"";
            return result;
        }

        [HttpPost]
        [Route("add")]
        public ApiResultViewModel<TransactionViewModel> Add(TransactionViewModel model) {
            var txn = new Transaction {
                AccountId = model.AccountId,
                CreateDate = model.CreateDate,
                Description = model.Description,
                DueDate = model.DueDate,
                HashtagId = model.HashtagId,
                HashtagValue = model.HashtagValue,
                MessageId = model.MessageId,
                Id = model.Id,
                ModifyDate = model.ModifyDate,
                PersonId = model.PersonId,
                Status = model.Status,
                Title = model.Title,
                TransactionType = model.TransactionType
            };
            var result = _service.Create(txn);
            if(result.Success)
                return new ApiResultViewModel<TransactionViewModel> {
                    model = model,
                    success = true,
                    message = result.Message
                };
            return new ApiResultViewModel<TransactionViewModel> {
                model = model,
                message = result.Message,
                success = false
            };
        }

        [HttpPost]
        [Route("update")]
        public async Task<ApiResultViewModel<TransactionViewModel>> Update(TransactionViewModel model) {
            if (model.Id <= 0)
                return new ApiResultViewModel<TransactionViewModel> {
                    success = false,
                    message = "Error: Entity Id is incorrect.",
                    model = null
                };
            var entity = await _service.SingleAsync(model.Id);
            if(entity == null)
                return new ApiResultViewModel<TransactionViewModel> {
                    success = false,
                    message = "Error: Entity is null.",
                    model = null
                };
            model.ModifyDate = DateTime.Now;

            var txn = new Transaction {
                AccountId = model.AccountId,
                CreateDate = entity.CreateDate,
                Description = model.Description,
                DueDate = model.DueDate,
                HashtagId = model.HashtagId,
                HashtagValue = model.HashtagValue,
                MessageId = model.MessageId,
                Id = model.Id,
                ModifyDate = DateTime.Now,
                PersonId = model.PersonId,
                Status = model.Status,
                Title = model.Title,
                TransactionType = model.TransactionType
            };
            _service.Update(txn, model.Id); //actual update
            return new ApiResultViewModel<TransactionViewModel> {
                model = model,
                message = $"The Entity with ID = '{model.Id}' has been updated.",
                success = true
            };
        }

        [HttpPost]
        [Route("delete/{id}")]
        public async Task<ApiResultViewModel<TransactionViewModel>> Delete(int id) {
            var entity = await _service.SingleAsync(id);
            var result = new ApiResultViewModel<TransactionViewModel>();
            try {
                var operation = _service.Delete(entity);
                if(operation.Success) {
                    result.success = true;
                    
                }

                return result;
            }
            catch (Exception e) {
                result.success = false;
                result.message = $"Error : {e.GetBaseException().Message}";
                return result;
            }
        }
    }
}