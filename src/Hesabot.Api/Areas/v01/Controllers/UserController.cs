﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hesabot.Core.Domain;
using Hesabot.Core.Services;
using Hesabot.Core.ViewModels;
using Hesabot.Core.ViewModels.Api;
using Mapster;
using Microsoft.AspNetCore.Mvc;

namespace Hesabot.Api.Areas.v01.Controllers {
    public class UserController : Controller {
        private IUserService _service;

        public UserController(IUserService service) {
            _service = service;
        }

        public IActionResult Index() {
            return View();
        }

        [HttpPost]
        [Route("get/{id}")]
        public async Task<ApiResultViewModel<UserViewModel>> Single(int id) {
            var entity = await _service.SingleAsync(id);
            return new ApiResultViewModel<UserViewModel> {
                model = entity.Adapt<UserViewModel>(),
                success = true
            };
        }

        [HttpPost]
        [Route("find/{username}")]
        public ApiResultViewModel<UserViewModel> Single(string username) {
            var entity = _service.GetByUsername(username);
            return new ApiResultViewModel<UserViewModel> {
                model = entity.Adapt<UserViewModel>(),
                success = true
            };
        }

        [HttpPost]
        [Route("register")]
        public ApiResultViewModel<UserViewModel> Register(UserViewModel model) {
            var entity = model.Adapt<User>();
            var result = _service.Create(entity);
            if(result.Success) {
                model.Id = result.Entity.Id;
                model.CreateDate = entity.CreateDate;
                model.ModifyDate = entity.ModifyDate;
                return new ApiResultViewModel<UserViewModel> {
                    model = model,
                    success = true
                };
            }

            return new ApiResultViewModel<UserViewModel> {
                model = model,
                message = $"Error : ",
                success = false
            };
        }

        [HttpPost]
        [Route("update")]
        public async Task<ApiResultViewModel<UserViewModel>> Update(UserViewModel model) {
            var result = new ApiResultViewModel<UserViewModel>();
            var entity = await _service.SingleAsync(model.Id);
            if(entity == null) {
                result.message = $"Entity with ID: {model.Id} not found.";
                result.model = model;
                return result;
            }
            model.CreateDate = entity.CreateDate;
            entity = model.Adapt<User>();
            entity.ModifyDate = DateTime.Now;
            _service.Update(entity, model.Id);
            return result;
        }
    }
}