﻿using System;
using Hesabot.Core.Domain.Base;

namespace Hesabot.Core.Domain {
    public class Transaction: BaseEntity {
        public DateTime DueDate { get; set; }
        public string Title { get; set; }
        public long Amount { get; set; }
        public string HashtagValue { get; set; }
        public long HashtagId { get; set; }
        public long? PersonId { get; set; }
        public long UserId { get; set; }
        public string Description { get; set; }
        public TransactionType TransactionType { get; set; }
        public long MessageId { get; set; }
        public long? AccountId { get; set; }

        #region Relations
        public virtual Account Account { get; set; }
        public virtual Person Person { get; set; }
        public virtual User User { get; set; }
        public virtual Hashtag Hashtag { get; set; }
        #endregion

    }

    public enum TransactionType
    {
        Expense = 0,
        Income,
        Transfer
    }
}
