﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hesabot.Core.Domain.Infrastructure {
    public class HesabotContext: DbContext {

        //public HesabotContext(DbContextOptions<FilmunContext> options) : base(options) {
        //}

        public HesabotContext(): base("HesabotConStr") { }

        #region Tables 
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Hashtag> Hashtags { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        #endregion


        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Entity<User>()
                .HasMany(x=> x.Transactions)
                .WithRequired(x=> x.User)
                .HasForeignKey(x=> x.UserId)
                .WillCascadeOnDelete();
            modelBuilder.Entity<User>()
                .HasMany(x=> x.Accounts)
                .WithRequired(x=> x.User)
                .HasForeignKey(x=> x.UserId)
                .WillCascadeOnDelete();
            modelBuilder.Entity<User>()
                .HasMany(x=> x.Persons)
                .WithRequired(x=> x.User)
                .HasForeignKey(x=> x.UserId)
                .WillCascadeOnDelete();
            modelBuilder.Entity<User>()
                .HasMany(x=> x.Hashtags)
                .WithRequired(x=> x.User)
                .HasForeignKey(x=> x.UserId)
                .WillCascadeOnDelete();
            modelBuilder.Entity<User>().ToTable("Users").HasKey(x=> x.Id);

            modelBuilder.Entity<Account>()
                .HasMany(x => x.Transactions)
                .WithRequired(x => x.Account)
                .HasForeignKey(x => x.AccountId);
            modelBuilder.Entity<Account>().ToTable("Accounts").HasKey(x=> x.Id);

            modelBuilder.Entity<Hashtag>()
                .HasMany(x => x.Transactions)
                .WithRequired(x => x.Hashtag)
                .HasForeignKey(x => x.HashtagId);
            modelBuilder.Entity<Hashtag>().ToTable("Hashtags").HasKey(x=> x.Id);

            modelBuilder.Entity<Person>()
                .HasMany(x => x.Transactions)
                .WithOptional(x => x.Person)
                .HasForeignKey(x => x.PersonId);
            modelBuilder.Entity<Person>().ToTable("People").HasKey(x=> x.Id);

            modelBuilder.Entity<Transaction>().ToTable("Transactions").HasKey(x=> x.Id);
        }

       
    }
}
