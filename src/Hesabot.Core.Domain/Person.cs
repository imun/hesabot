﻿using System.Collections.Generic;
using Hesabot.Core.Domain.Base;

namespace Hesabot.Core.Domain {
    public class Person: BaseEntity {
        public string Title { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public long UserId { get; set; }

        #region Relations
        public virtual User User { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        #endregion
    }
}
