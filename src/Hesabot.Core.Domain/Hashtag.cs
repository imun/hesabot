﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.Domain.Base;

namespace Hesabot.Core.Domain {
    public class Hashtag: BaseEntity {
        public string Title { get; set; }
        public long? ParentId { get; set; }
        public string Description { get; set; }
        public long UserId { get; set; }

        #region Relations
        public virtual User User { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        #endregion  

    }
}
