﻿using System;
namespace Hesabot.Core.Domain.Base {
    public class BaseEntity {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Enable;
    }
}
