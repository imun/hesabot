﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Hesabot.Core.Domain.Base;

namespace Hesabot.Core.Domain {
    public class User : BaseEntity {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime? WebRegisterDate { get; set; }
        public string TelegramId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [NotMapped]
        public string Fullname {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }


        #region Relations
        public virtual ICollection<Account> Accounts { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
        public virtual ICollection<Hashtag> Hashtags { get; set; }
        public virtual ICollection<Person> Persons { get; set; }

        #endregion
    }
}
