﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hesabot.Core.Repository {
    public interface IHesabotRepository<T> where T: class {
        Task<T> AddAsync(T entity);
        T Add(T entity);
        Task<T> UpdateAsync(T entity, object id);
        T Update(T entity, object id);
        void Delete(T entity);
        IQueryable<T> FindAll();
        Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression);
        ICollection<T> Query(Expression<Func<T, bool>> expression);
        T Single(object id);
        Task<T> SingleAsync(object id);
        Task<T> SingleOrDefaultAsync(Expression<Func<T, bool>> expression);
        T SingleOrDefault(Expression<Func<T, bool>> expression);
        int Save();
        Task<int> SaveAsync();
        IQueryable<T> Select();
        DbSet<TEntity> GetTable<TEntity>() where TEntity : class;
        DbContext GetDbContext();
    }
}
