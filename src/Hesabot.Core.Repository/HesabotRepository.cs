﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hesabot.Core.Domain.Infrastructure;

namespace Hesabot.Core.Repository {
    public class HesabotRepository<T>: IHesabotRepository<T> where T: class {

        private readonly HesabotContext _db;
        public DbSet<T> Table;

        public HesabotRepository(HesabotContext db) {
            _db = db;
            Table = _db.Set<T>();
        }

        public virtual void Delete(T entity) {
            Table.Remove(entity);
        }

        public virtual IQueryable<T> FindAll() {
            return Table;
        }


        public virtual T Single(object id) {
            return Table.Find(id);
        }

        public virtual async Task<T> SingleAsync(object id) {
            return await Table.FindAsync(id);
        }

        public virtual async Task<T> AddAsync(T entity) {
            Table.Add(entity);
            await SaveAsync();
            return entity;
        }

        public virtual T Add(T entity) {
            Table.Add(entity);
            Save();
            return entity;
        }

        public virtual async Task<T> UpdateAsync(T entity, object id) {
            if (entity == null) return null;
            T exist = Table.Find(id);
            if (exist != null) {
                _db.Entry<T>(exist).CurrentValues.SetValues(entity);
                await SaveAsync();
            }
            return exist;
        }

        public virtual T Update(T entity, object id) {
            if (entity == null) return null;
            T exist = Table.Find(id);
            if (exist != null) {
                _db.Entry<T>(exist).CurrentValues.SetValues(entity);
                Save();
            }
            return exist;
        }

        public virtual async Task<ICollection<T>> QueryAsync(Expression<Func<T, bool>> expression) {
            return await Table.Where(expression).ToListAsync();
        }

        public virtual async Task<T> SingleOrDefaultAsync(Expression<Func<T, bool>> expression) {
            return await Table.SingleOrDefaultAsync(expression);
        }

        public virtual T SingleOrDefault(Expression<Func<T, bool>> expression) {
            return Table.SingleOrDefault(expression);
        }

        public ICollection<T> Query(Expression<Func<T, bool>> expression) {
            return Table.Where(expression).ToList();
        }

        public virtual async Task<int> SaveAsync() {
            return await _db.SaveChangesAsync();
        }

        public virtual int Save() {
            return _db.SaveChanges();
        }

        public virtual IQueryable<T> Select() {
            return Table.AsNoTracking().AsQueryable<T>();
        }

        public virtual DbSet<TEntity> GetTable<TEntity>() where TEntity : class {
            return _db.Set<TEntity>();
        }

        public DbContext GetDbContext() {
            return _db;
        }
    }
}
