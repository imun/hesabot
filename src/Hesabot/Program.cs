﻿using System;
using System.Windows.Forms;
using Hesabot.Core.Domain;
using Hesabot.Core.Domain.Infrastructure;
using Hesabot.Core.Repository;
using Hesabot.Core.Services;
using Hesabot.Core.Services.Infrastructure;
using Hesabot.Core.Services.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Hesabot {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ConfigureServices();
            MapsterConfiguration.Configure();
            Application.Run(new MainForm());
        }

        public static IServiceProvider ServiceProvider { get; set; }

        static void ConfigureServices() {
            ServiceProvider = new ServiceCollection()
                .AddSingleton<HesabotContext>()
                .AddTransient<IValidationResult, ValidationResult>()
                .AddSingleton<IHesabotRepository<Transaction>, HesabotRepository<Transaction>>()
                .AddSingleton<IHesabotRepository<Person>, HesabotRepository<Person>>()
                .AddSingleton<IHesabotRepository<Account>, HesabotRepository<Account>>()
                .AddSingleton<IHesabotRepository<Hashtag>, HesabotRepository<Hashtag>>()
                .AddSingleton<IHesabotRepository<User>, HesabotRepository<User>>()
                .AddSingleton<ITransactionService, TransactionService>()
                .AddSingleton<IPersonService, PersonService>()
                .AddSingleton<IAccountService, AccountService>()
                .AddSingleton<IHashtagService, HashtagService>()
                .AddSingleton<IUserService, UserService>()
                .BuildServiceProvider();
        }
    }
}
