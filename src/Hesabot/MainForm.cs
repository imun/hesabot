﻿using System;
using System.Windows.Forms;
using Hesabot.Bot;

namespace Hesabot {
    public partial class MainForm : Form {
        public TelegramBot Hesabot { get; set; }

        public MainForm() {
            InitializeComponent();
            Hesabot = new TelegramBot();
            Hesabot.Start();
        }

        private void MainForm_Load(object sender, EventArgs e) {
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e) {
            e.Cancel = true;
            Hide();
        }
    }
}