﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels {
    public class HashtagViewModel : BaseEntityViewModel {
        public string Title { get; set; }
        public int? ParentId { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }

        #region Relations
        public string UserDisplay { get; set; }
        public virtual List<TransactionViewModel> Transactions { get; set; }
        #endregion
    }
}
