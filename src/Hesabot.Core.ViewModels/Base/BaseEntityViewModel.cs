﻿using System;
using Hesabot.Core.Domain.Base;
using Farasun.Library.PersianTools.DateTime;

namespace Hesabot.Core.ViewModels.Base {
    public abstract class BaseEntityViewModel {
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public EntityStatus Status { get; set; } = EntityStatus.Enable;
        public string CreateDateDisplay => CreateDate.ToPersianDateString(PersianDateFormat.DayNum_MonthName_Year);
        public string ModifyDateDisplay => ModifyDate.ToPersianDateString(PersianDateFormat.DayNum_MonthName_Year);
    }
}
