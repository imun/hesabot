﻿using System;
using Hesabot.Core.Domain;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels {
    public class TransactionViewModel: BaseEntityViewModel {
        public string Title { get; set; }
        public DateTime DueDate { get; set; }
        public long Amount { get; set; }
        public string HashtagValue { get; set; }
        public long HashtagId { get; set; }
        public long? PersonId { get; set; }
        public long UserId { get; set; }
        public string Description { get; set; }
        public TransactionType TransactionType { get; set; }
        public long MessageId { get; set; }
        public long? AccountId { get; set; }
        public string AccountTitle { get; set; }
        public string PersonTitle { get; set; }
    }
}
