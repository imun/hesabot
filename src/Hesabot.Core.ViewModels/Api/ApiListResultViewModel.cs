﻿using System.Collections.Generic;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels.Api {
    public class ApiListResultViewModel<T> where T: BaseEntityViewModel {
        public ApiListResultViewModel() {
            items = new List<T>();
        }
        public ApiListResultViewModel(IList<T> _items) {
            items = _items;
        }
        public IList<T> items { get; set; }
        public int totalCount => items.Count;
        public bool success { get; set; }
        public string message { get; set; }
    }
}
