﻿using System;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels.Api {
    public class ApiResultViewModel<T> where T: BaseEntityViewModel {
        public string message { get; set; }
        public bool success { get; set; }
        public T model { get; set; }
    }
}
