﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels {
    public class PersonViewModel: BaseEntityViewModel {
        public string Title { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
    }
}
