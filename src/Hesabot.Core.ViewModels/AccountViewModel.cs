﻿using System.Collections.Generic;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels {
    public class AccountViewModel: BaseEntityViewModel {
        public string Title { get; set; }
        public bool Active { get; set; }
        public string AccountNumber { get; set; }
        public string CardNumber { get; set; }
        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string Address { get; set; }
        public long InitBalance { get; set; }
        public string Description { get; set; }
        public long UserId { get; set; }
        public bool IsDefault { get; set; }

        #region Relations
        public virtual List<TransactionViewModel> Transactions { get; set; }
        //public virtual User User { get; set; }
        #endregion
    }
}
