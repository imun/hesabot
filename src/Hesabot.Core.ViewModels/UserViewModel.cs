﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hesabot.Core.Domain;
using Hesabot.Core.ViewModels.Base;

namespace Hesabot.Core.ViewModels {
    public class UserViewModel: BaseEntityViewModel {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public DateTime? WebRegisterDate { get; set; }
        public string TelegramId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Fullname {
            get { return string.Format("{0} {1}", FirstName, LastName); }
        }

        #region Navigations 
        public List<TransactionViewModel> Transactions { get; set; }
        public List<Account> Accounts { get; set; }
        #endregion 
    }
}
